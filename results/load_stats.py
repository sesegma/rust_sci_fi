import glob
import json

import numpy as np


class StatHolder:
    def __init__(self):
        self.chi2_x = []
        self.chi2_y = []
        self.true_chi2_x = []
        self.true_chi2_y = []
        self.perc_found_full = []
        self.found_full = []
        self.true_full = []
        self.false_full = []
        self.false_full_single = 0
        self.perc_found_oct = []
        self.found_oct = []
        self.true_oct = []
        self.false_oct = 0
        self.missed_oct_keys = []
        self.missed_full_keys = []
        self.missed_electrons = []
        self.found_electrons = []
        self.pull_x = []
        # self.pull_x_x_layers = []
        self.pull_y = []
        self.resolution_x = []
        self.resolution_y = []
        self.size = []
        self.ft_size = []
        self.found_full_keys = []
        self.event_num = []
        self.time = []
        self.quad1_time = []
        self.quad2_time = []
        self.quad3_time = []
        self.oct_time = []
        self.full_track_time = []
        self.oct_chi2_x = []
        self.oct_chi2_y = []
        self.oct_true_chi2_x = []
        self.oct_true_chi2_y = []
        self.missing_partileIDs = []
        self.ghost_full = []
        self.x_proj_diff = []
        self.tx_diff = []
        self.tx_diff_rel = []
        self.true_x_proj_diff_parabolic = []
        self.true_x_proj_diff_linear = []
        self.true_tx_diff = []
        self.true_charges = []
        self.true_momentum = []
        self.omega = []
        # self.true_tx_diff_linear = []

    def convert_to_numpy(self):
        for item in self.__dict__:
            if isinstance(getattr(self, item), list):
                setattr(self, item, np.asarray(getattr(self, item)))


def load_stats(fnames):
    stat_holder = StatHolder()
    for filename in fnames:
        with open(filename) as infile:
            if 'params' in filename:
                continue
            data = json.load(infile)
            full_track_data = data['full_track_stats']
            oct_data = data['oct_stats']
            stat_holder.oct_chi2_x.extend(oct_data['chi2_x'])
            stat_holder.oct_chi2_y.extend(oct_data['chi2_y'])
            stat_holder.oct_true_chi2_x.extend(oct_data['true_chi2_x'])
            stat_holder.oct_true_chi2_y.extend(oct_data['true_chi2_y'])
            stat_holder.event_num.append(full_track_data['event_number'])
            stat_holder.chi2_x.extend(full_track_data['chi2_x'])
            stat_holder.chi2_y.extend(full_track_data['chi2_y'])
            stat_holder.true_chi2_x.extend(full_track_data['true_chi2_x'])
            stat_holder.true_chi2_y.extend(full_track_data['true_chi2_y'])
            stat_holder.size.append(full_track_data['size'])
            stat_holder.time.append(full_track_data['time'])
            stat_holder.quad1_time.append(full_track_data['quad1_time'])
            stat_holder.quad2_time.append(full_track_data['quad2_time'])
            stat_holder.quad3_time.append(full_track_data['quad3_time'])
            stat_holder.oct_time.append(full_track_data['oct_time'])
            stat_holder.full_track_time.append(
                full_track_data['full_track_time'])
            stat_holder.missed_full_keys.append(
                full_track_data['missed_full_keys'])
            stat_holder.missing_partileIDs.extend(
                full_track_data['missed_particles_ids'])
            stat_holder.missed_electrons.append(
                full_track_data['missed_electrons'])
            stat_holder.found_electrons.append(
                full_track_data['found_electrons'])
            stat_holder.missed_oct_keys.append(oct_data['missed_oct_keys'])
            stat_holder.ft_size.append(full_track_data['ft_size'])
            #         print(full_track_data['size'])
            stat_holder.found_full_keys.append(
                full_track_data['found_full_keys'])

            stat_holder.x_proj_diff.extend(full_track_data['x_proj_diff'])
            stat_holder.tx_diff.extend(full_track_data['tx_diff'])
            stat_holder.tx_diff_rel.extend(full_track_data['tx_diff_rel'])
            stat_holder.true_x_proj_diff_linear.append(
                full_track_data['true_x_proj_diff_linear'])
            stat_holder.true_x_proj_diff_parabolic.append(
                full_track_data['true_x_proj_diff_parabolic'])
            stat_holder.true_tx_diff.append(full_track_data['true_tx_diff'])

            if len(full_track_data['true_full_keys']) > 0:
                # stat_holder.true_charges.append(
                #     full_track_data['true_charges'])
                stat_holder.found_full.append(
                    full_track_data['found_full_keys'])
                stat_holder.pull_x.extend(full_track_data['pull_x'])
                # stat_holder.pull_x_x_layers.extend(
                #     full_track_data['pull_x_x_layers'])
                stat_holder.pull_y.extend(full_track_data['pull_y'])
                stat_holder.resolution_x.extend(
                    full_track_data['resolution_x'])
                stat_holder.resolution_y.extend(
                    full_track_data['resolution_y'])
                stat_holder.true_full.append(full_track_data['true_full_keys'])
                stat_holder.false_full_single += (
                    full_track_data['size'] - len(
                        full_track_data['found_full_keys']))
                stat_holder.false_full.append((full_track_data['size'] - len(full_track_data['found_full_keys'])) \
                /full_track_data['size'])
                stat_holder.ghost_full.append((full_track_data['size'] - len(
                    full_track_data['found_full_keys'])))
                #             print(false_full)
                # stat_holder.true_momentum.extend(
                #     full_track_data['true_momentum'])
                # stat_holder.omega.extend(full_track_data['omega'])
            if len(oct_data['true_oct_keys']) > 0:
                stat_holder.found_oct.append(oct_data['found_oct_keys'])
                stat_holder.true_oct.append(oct_data['true_oct_keys'])
                stat_holder.false_oct += (
                    oct_data['size'] - len(oct_data['found_oct_keys']))
    #             false_oct.append((oct_data['size'] - len(oct_data['found_oct_keys']))\
    #                              /oct_data['size'])
    #             found_oct.append(len(oct_data['found_oct_keys'])/len(oct_data['true_oct_keys']))
    stat_holder.missing_partileIDs = np.array(stat_holder.missing_partileIDs)
    stat_holder.convert_to_numpy()
    return stat_holder


class SpeedUpStats:
    def __init__(self):
        self.time = []
        self.size = 0

    def add_event_number(self, event_number):
        self.event_number = event_number


def load_times():
    fnames = glob.glob('scaling_time_data/*')
    stat1 = SpeedUpStats()
    stat2 = SpeedUpStats()
    stat4 = SpeedUpStats()
    stat8 = SpeedUpStats()
    stat16 = SpeedUpStats()
    for filename in fnames:
        with open(filename) as infile:
            content = [line.strip() for line in infile.readlines()]
            stat1.size = int(content[1])
            stat2.size = int(content[4])
            stat4.size = int(content[7])
            stat8.size = int(content[10])
            stat16.size = int(content[13])
            stat1.time.append(float(content[2]))
            stat2.time.append(float(content[5]))
            stat4.time.append(float(content[8]))
            stat8.time.append(float(content[11]))
            stat16.time.append(float(content[14]))
            # if not stat.size:
            #     stat.size.extend(
            #         content[i] for i in range(len(content)) if i % 3 == 1)
            # stat.time1.append(content[2])
    return (stat1, stat2, stat4, stat8, stat16)
