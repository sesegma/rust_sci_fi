// use itertools::Itertools;
use reconstruction::data_handler::hit_combinations::eval_parametrization_x;
use reconstruction::data_handler::{EventData, FullTrack, KeyType, McFTHit, Octuplet, Precision};
use std::collections::HashSet;
use std::fmt;

const X_LAYERS: [u8; 6] = [0, 3, 4, 7, 8, 11];
const UV_LAYERS: [u8; 6] = [1, 2, 5, 6, 9, 10];

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct OctStats {
    size: usize,
    true_oct_keys: Vec<KeyType>,
    found_oct_keys: Vec<KeyType>,
    missed_oct_keys: Vec<KeyType>,
    missed_particles_ids: Vec<i32>,
    electron_keys: Vec<KeyType>,
    true_chi2_x: Vec<Precision>,
    true_chi2_y: Vec<Precision>,
    chi2_x: Vec<Precision>,
    chi2_y: Vec<Precision>,
    time: f64,
}

impl fmt::Display for OctStats {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Number of true oct keys: {}
            \rNumber of found oct keys: {}
            \rNumber of missed oct keys: {}
            \rTotal number of octuplets: {}
            \rMissed key ids: {:?}
            \rMissed particles ids: {:?}",
            self.true_oct_keys.len(),
            self.found_oct_keys.len(),
            self.missed_oct_keys.len(),
            self.size,
            self.missed_oct_keys,
            self.missed_particles_ids,
        )
    }
}

impl OctStats {
    pub fn new(event_data: &EventData, octs: &[Octuplet], time: f64) -> Self {
        let layers = get_layers_from_octuplet(&octs[0]);
        // let layers: HashSet<_> = (4..12).collect();
        // println!("Oct layers {:?}", layers);

        let generated_keys: HashSet<KeyType> = event_data
            .FT
            .hits
            .iter()
            .filter_map(|hit| {
                if hit.mcp_key != -1 {
                    Some(hit.mcp_key)
                } else {
                    None
                }
            })
            .collect();
        // dbg!(&generated_keys);
        let mut true_oct_keys: HashSet<KeyType> = HashSet::new();
        for &key in generated_keys.iter() {
            let hit_layers: HashSet<u8> = event_data
                .FT
                .hits
                .iter()
                .filter(|hit| hit.mcp_key == key && hit.hit_zone == event_data.hit_zone)
                .map(|group_hit| group_hit.layer)
                .collect();
            if hit_layers.is_superset(&layers) {
                true_oct_keys.insert(key);
            }
        }
        let oct_keys: HashSet<_> = octs
            .iter()
            .filter_map(|track| {
                if track.true_track {
                    track.majority_key
                } else {
                    None
                }
            })
            .collect();

        // let (_, oct_keys) = Self::get_found_oct_keys(&octs);
        let found_keys: HashSet<_> = true_oct_keys.intersection(&oct_keys).cloned().collect();
        let found_oct_keys: Vec<_> = found_keys.clone().into_iter().collect();
        // println!("{:?}", found_oct_keys);

        let mut missed_keys = true_oct_keys.difference(&oct_keys).cloned().collect::<Vec<_>>();
        missed_keys.sort();
        let missed_particles_ids = missed_keys
            .iter()
            .flat_map(|missed_key| {
                event_data.MC.particles.iter().find_map(move |particle| {
                    if particle.key == *missed_key {
                        Some(particle.particleID)
                    } else {
                        None
                    }
                })
            })
            .collect();
        let missed_oct_keys = missed_keys;
        let true_oct_keys = true_oct_keys.clone().into_iter().collect::<Vec<_>>();
        let mut stats = Self {
            size: octs.len(),
            true_oct_keys,
            found_oct_keys,
            missed_oct_keys,
            missed_particles_ids,
            electron_keys: vec![],
            true_chi2_x: vec![],
            true_chi2_y: vec![],
            chi2_x: vec![],
            chi2_y: vec![],
            time,
        };
        for track in octs.iter() {
            if track.true_track {
                stats.true_chi2_x.push(track.chi2_x);
                stats.true_chi2_y.push(track.chi2_y);
            } else {
                stats.chi2_x.push(track.chi2_x);
                stats.chi2_y.push(track.chi2_y);
            }
        }
        stats
    }

    pub fn append(&mut self, mut other: OctStats) {
        self.size += other.size;
        self.true_oct_keys.append(&mut other.true_oct_keys);
        self.found_oct_keys.append(&mut other.found_oct_keys);
        self.missed_oct_keys.append(&mut other.missed_oct_keys);
        self.missed_particles_ids.append(&mut other.missed_particles_ids);
        self.electron_keys.append(&mut other.electron_keys);
        self.true_chi2_x.append(&mut other.true_chi2_x);
        self.true_chi2_y.append(&mut other.true_chi2_y);
        self.chi2_x.append(&mut other.chi2_x);
        self.chi2_y.append(&mut other.chi2_y);
        self.time += other.time;
    }

    // fn get_found_oct_keys<'a>(octs: &'a [Octuplet]) -> (Vec<&'a Octuplet<'a>>, HashSet<KeyType>) {
    //     let mut oct_keys = HashSet::new();
    //     let found_octs = get_tracks_from_octs(octs);
    //     for oct in found_octs.iter() {
    //         if let Some(majority_key) = oct.majority_key {
    //             oct_keys.insert(majority_key);
    //         }
    //     }
    //     (found_octs, oct_keys)
    // }
}

#[derive(Debug, Default)]
pub struct QuadStats {
    size: usize,
    true_quad_keys: Vec<KeyType>,
    found_quad_keys: Vec<KeyType>,
    missed_quad_keys: Vec<KeyType>,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct FullTrackStats {
    pub event_number: u32,
    pub size: usize,
    pub ft_size: usize,
    pub true_full_keys: Vec<KeyType>,
    pub found_full_keys: Vec<KeyType>,
    pub missed_full_keys: Vec<KeyType>,
    pub found_electrons: Vec<KeyType>,
    pub missed_electrons: Vec<KeyType>,
    pub true_chi2_x: Vec<Precision>,
    pub true_chi2_y: Vec<Precision>,
    pub chi2_x: Vec<Precision>,
    pub chi2_y: Vec<Precision>,
    pub pull_x: Vec<Precision>,
    pub resolution_x: Vec<Precision>,
    pub pull_y: Vec<Precision>,
    pub resolution_y: Vec<Precision>,
    pub time: f64,
    pub quad1_time: f64,
    pub quad2_time: f64,
    pub quad3_time: f64,
    pub oct_time: f64,
    pub full_track_time: f64,
    pub missed_particles_ids: Vec<i32>,
    pub x_proj_diff: Vec<Precision>,
    pub tx_diff: Vec<Precision>,
    pub tx_diff_rel: Vec<Precision>,
    pub true_x_proj_diff_parabolic: Vec<Precision>,
    pub true_x_proj_diff_linear: Vec<Precision>,
    pub true_tx_diff: Vec<i32>,
    pub true_charges: Vec<i32>,
    pub omega: Vec<Precision>,
    pub true_momentum: Vec<(Precision, Precision, Precision)>,
}

impl fmt::Display for FullTrackStats {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Number of true keys: {}
            \rNumber of found all full keys: {}
            \rNumber of missed keys: {}
            \rTotal number of full tracks: {}
            \rTotal number of false tracks: {}
            \rFound full keys ids: {:?},
            \rMissed keys ids {:?}
            \rMissed particle ids {:?}
            \rMax chi-squared X {:?}
            \rMax chi_squared Y {:?}
            \rFull track building time: {:5.4} ms",
            self.true_full_keys.len(),
            self.found_full_keys.len(),
            self.missed_full_keys.len(),
            self.size,
            self.size - self.found_full_keys.len(),
            self.found_full_keys,
            self.missed_full_keys,
            self.missed_particles_ids,
            // self.chi2_x,
            // self.chi2_y,
            self.true_chi2_x.iter().fold(0.0, |max, &f| Precision::max(max, f)),
            self.true_chi2_y.iter().fold(0.0, |max, &f| Precision::max(max, f)),
            self.time * 1e3
        )
    }
}

impl FullTrackStats {
    pub fn from_event_number(event_number: u32) -> Self {
        let mut s = Self::default();
        s.event_number = event_number;
        s
    }
    pub fn new(
        event_data: &EventData,
        full_tracks: &[FullTrack],
        algo_time: (f64, f64, f64, f64, f64, f64),
    ) -> Self {
        let mut stats = Self {
            event_number: event_data.odin.1,
            size: full_tracks.len(),
            ft_size: event_data.FT.hits.len(),
            true_full_keys: vec![],
            found_full_keys: vec![],
            missed_full_keys: vec![],
            found_electrons: vec![],
            missed_electrons: vec![],
            true_chi2_x: vec![],
            true_chi2_y: vec![],
            chi2_x: vec![],
            chi2_y: vec![],
            pull_x: vec![],
            resolution_x: vec![],
            // pull_x_x_layers: vec![],
            pull_y: vec![],
            resolution_y: vec![],
            quad1_time: algo_time.0,
            quad2_time: algo_time.1,
            quad3_time: algo_time.2,
            oct_time: algo_time.3,
            full_track_time: algo_time.4,
            time: algo_time.5,
            missed_particles_ids: vec![],
            x_proj_diff: vec![],
            tx_diff: vec![],
            tx_diff_rel: vec![],
            true_x_proj_diff_parabolic: vec![],
            true_x_proj_diff_linear: vec![],
            true_tx_diff: vec![],
            true_charges: vec![],
            omega: vec![],
            true_momentum: vec![],
        };
        stats.x_proj_diff =
            full_tracks.iter().map(|track| track.x_proj_parabolic - track.x_proj_linear).collect();
        stats.tx_diff =
            full_tracks.iter().map(|track| (track.tx_parabolic - track.tx_linear)).collect();
        stats.tx_diff_rel = full_tracks
            .iter()
            .map(|track| (track.tx_parabolic - track.tx_linear) / track.track_params.x_slope)
            .collect();

        let layers: HashSet<_> = (0..12).collect::<HashSet<u8>>();
        // get the true keys that have hits in all 12 layers
        let generated_keys: HashSet<KeyType> = event_data
            .FT
            .hits
            .iter()
            .filter_map(|hit| {
                if hit.mcp_key != -1 {
                    Some(hit.mcp_key)
                } else {
                    None
                }
            })
            .collect();
        let mc_particle_keys: HashSet<_> =
            event_data.MC.particles.iter().map(|part| part.key).collect();
        let generated_keys: HashSet<_> = mc_particle_keys.intersection(&generated_keys).collect();

        // get the keys of tracks that have reconstructed hits in all 12 layers
        let mut true_full_keys: HashSet<KeyType> = HashSet::new();
        for &key in generated_keys.iter() {
            let hit_layers: HashSet<u8> = event_data
                .FT
                .hits
                .iter()
                .filter(|&hit| hit.mcp_key == *key && hit.hit_zone == event_data.hit_zone)
                .map(|group_hit| group_hit.layer)
                .collect();
            if hit_layers == layers {
                true_full_keys.insert(*key);
            }
        }

        let full_keys: HashSet<_> = full_tracks
            .iter()
            .filter_map(|track| {
                if track.true_track {
                    track.majority_key
                } else {
                    None
                }
            })
            .collect();
        // find keys that are true and have reconstructed hits in all 12 layers
        let found_keys: HashSet<_> = full_keys.intersection(&true_full_keys).cloned().collect();
        stats.found_full_keys = found_keys.clone().into_iter().collect();

        let mut missed_keys = true_full_keys.difference(&full_keys).cloned().collect::<Vec<_>>();
        missed_keys.sort();
        stats.missed_full_keys = missed_keys;
        for track in full_tracks.iter() {
            if track.true_track {
                stats.true_chi2_x.push(track.chi2_x);
                stats.true_chi2_y.push(track.chi2_y);
            } else {
                stats.chi2_x.push(track.chi2_x);
                stats.chi2_y.push(track.chi2_y);
            }
        }
        stats.missed_particles_ids = stats
            .missed_full_keys
            .iter()
            .flat_map(|missed_key| {
                event_data.MC.particles.iter().find_map(move |particle| {
                    if particle.key == *missed_key {
                        Some(particle.particleID)
                    } else {
                        None
                    }
                })
            })
            .collect();

        // get the electron data
        for particle in event_data.MC.particles.iter() {
            if particle.particleID.abs() == 11 {
                if found_keys.contains(&particle.key) {
                    stats.found_electrons.push(particle.key);
                } else if stats.missed_full_keys.contains(&particle.key) {
                    stats.missed_electrons.push(particle.key);
                    // dbg!(particle.key);
                }
            }
        }
        stats.true_full_keys = true_full_keys.clone().into_iter().collect();
        stats.true_full_keys.sort();

        for track in full_tracks.iter() {
            if let Some(track_key) = track.majority_key {
                match event_data.MC.particles.iter().find(|particle| particle.key == track_key) {
                    Some(partic) => {
                        stats.true_momentum.push((partic.px, partic.py, partic.pz));
                        stats.omega.push(track.track_params.x_quadratic)
                    }
                    None => {}
                }
            }
        }
        for track in full_tracks.iter() {
            if track.true_track {
                let real_coords = event_data
                    .MC
                    .FT_hits
                    .iter()
                    .filter(|hit| {
                        if let Some(key) = track.majority_key {
                            hit.mcp_key == key
                        } else {
                            false
                        }
                    })
                    .collect::<Vec<&McFTHit>>();
                if real_coords.len() == 12 {
                    for ((z_val, x_val, w), hit) in
                        track.get_vec_of_xw().iter().zip(real_coords.iter())
                    {
                        if (z_val - hit.z).abs() < 10.0 {
                            stats.resolution_x.push(x_val - hit.x);
                            stats.pull_x.push((x_val - hit.x) * w.sqrt());
                            //The part below deals with projection onto the 4th layer
                            if hit.layer == 3 {
                                stats
                                    .true_x_proj_diff_parabolic
                                    .push(hit.x - track.x_proj_parabolic);
                                stats.true_x_proj_diff_linear.push(hit.x - track.x_proj_linear);
                                stats.true_charges.push(
                                    event_data
                                        .MC
                                        .particles
                                        .iter()
                                        .filter(|particle| particle.key == hit.mcp_key)
                                        .map(|p| p.particleID)
                                        .next()
                                        .unwrap(),
                                )
                            }
                        }
                    }
                    // this is nonsense
                    if let Some(real_ys) = Self::get_yv_y(&real_coords) {
                        for ((y_val, w), hit_y) in track.get_vec_of_yw().iter().zip(real_ys.iter())
                        {
                            if *hit_y < 2700.0 {
                                stats.pull_y.push((y_val - hit_y) * w.sqrt());
                                stats.resolution_y.push(y_val - hit_y);
                            }
                        }
                    }
                }
            }
        }
        stats
    }

    fn get_yv_y(real_coords: &[&McFTHit]) -> Option<Vec<Precision>> {
        let uv_z_vals = [7895.0, 7966.0, 8577.0, 8648.0, 9262.0, 9333.0];
        uv_z_vals
            .iter()
            .enumerate()
            .map(|(_, val)| {
                real_coords.iter().find_map(|hit| {
                    if (hit.z - val).abs() < 20.0 {
                        Some(hit.y)
                    } else {
                        None
                    }
                })
            })
            .collect()
    }

    pub fn append(&mut self, mut other: FullTrackStats) {
        self.size += other.size;
        self.ft_size = if self.ft_size < 1 {
            other.ft_size
        } else {
            self.ft_size
        };
        self.true_full_keys.append(&mut other.true_full_keys);
        self.found_full_keys.append(&mut other.found_full_keys);
        self.missed_full_keys.append(&mut other.missed_full_keys);
        self.found_electrons.append(&mut other.found_electrons);
        self.missed_electrons.append(&mut other.missed_electrons);
        self.true_chi2_x.append(&mut other.true_chi2_x);
        self.true_chi2_y.append(&mut other.true_chi2_y);
        self.chi2_x.append(&mut other.chi2_x);
        self.chi2_y.append(&mut other.chi2_y);
        self.pull_x.append(&mut other.pull_x);
        // self.pull_x_x_layers.append(&mut other.pull_x_x_layers);
        self.resolution_x.append(&mut other.resolution_x);
        self.pull_y.append(&mut other.pull_y);
        self.resolution_y.append(&mut other.resolution_y);
        self.time += other.time;
        self.quad1_time += other.quad1_time;
        self.quad2_time += other.quad2_time;
        self.quad3_time += other.quad3_time;
        self.oct_time += other.oct_time;
        self.full_track_time += other.full_track_time;
        self.missed_particles_ids.append(&mut other.missed_particles_ids);
        self.x_proj_diff.append(&mut other.x_proj_diff);
        self.tx_diff.append(&mut other.tx_diff);
        self.tx_diff_rel.append(&mut other.tx_diff_rel);
        self.true_x_proj_diff_parabolic.append(&mut other.true_x_proj_diff_parabolic);
        self.true_x_proj_diff_linear.append(&mut other.true_x_proj_diff_linear);
        self.true_tx_diff.append(&mut other.true_tx_diff);
        self.true_charges.append(&mut other.true_charges);
        self.omega.append(&mut other.omega);
        self.true_momentum.append(&mut other.true_momentum);
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AllStats {
    oct_stats: OctStats,
    full_track_stats: FullTrackStats,
}

impl AllStats {
    pub fn new(oct_stats: OctStats, full_track_stats: FullTrackStats) -> Self {
        Self {
            oct_stats,
            full_track_stats,
        }
    }
}

fn get_layers_from_octuplet(oct: &Octuplet) -> HashSet<u8> {
    let mut layers = HashSet::new();
    layers.insert(oct.quad_up.hit1.layer);
    layers.insert(oct.quad_up.hit2.layer);
    layers.insert(oct.quad_up.hit3.layer);
    layers.insert(oct.quad_up.hit4.layer);
    layers.insert(oct.quad_down.hit1.layer);
    layers.insert(oct.quad_down.hit2.layer);
    layers.insert(oct.quad_down.hit3.layer);
    layers.insert(oct.quad_down.hit4.layer);
    layers
}
