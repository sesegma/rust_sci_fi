#![allow(dead_code)]
// mod competition;
mod stats;
#[macro_use]
extern crate serde_derive;

// use competition::*;
use colored::*;
use rayon::prelude::*;
use reconstruction::{
    args::{Opt, Parameters},
    competition::*,
    data_handler::*,
};
use stats::{AllStats, FullTrackStats, OctStats};
use std::fs;
use time::precise_time_s;

use std::error::Error;
use std::fs::OpenOptions;
use std::io::prelude::*;
use std::path::PathBuf;
use walkdir::WalkDir;

macro_rules! timing {
    ($x:expr) => {{
        let start_time = precise_time_s();
        let result = $x;
        let delta = precise_time_s() - start_time;
        // println!("{} took : {:>5.4}ms", stringify!($x), delta * 1e3);
        (result, delta)
    }};
}

fn run_all(params: Parameters, opt_dir: Option<PathBuf>) -> std::io::Result<()> {
    let manifest = env!("CARGO_MANIFEST_DIR");
    let mut stat_destination: PathBuf = env!("CARGO_MANIFEST_DIR").into();
    let mut track_destination: PathBuf = env!("CARGO_MANIFEST_DIR").into();
    let (stat_dir, track_dir) = {
        if let Some(dir_name) = opt_dir {
            // manifest_2.push(stat_destination)
            stat_destination.push(dir_name.clone());
            track_destination.push(dir_name.clone());
            stat_destination.push("stats/");
            track_destination.push("tracks/");
            match (fs::create_dir_all(&stat_destination), fs::create_dir_all(&track_destination)) {
                (Ok(_), Ok(_)) => (
                    stat_destination.clone().to_str().unwrap().into(),
                    track_destination.clone().to_str().unwrap().into(),
                ),
                _ => {
                    println!("Couldn't create folder {:?}, exiting the program.", dir_name);
                    // println!("{}: {}", "Error".bold().red(), e);
                    std::process::exit(1);
                }
            }
        } else {
            (format!("{}/results/stats", manifest), format!("{}/found_tracks", manifest))
        }
    };

    let mut params_file =
        OpenOptions::new().write(true).create(true).open(format!("{}/params.json", stat_dir))?;
    let serialized_params = serde_json::to_string(&params).unwrap();
    write!(params_file, "{}", serialized_params)?;

    let mut counter = 0;
    let mut total_time = 0.0;
    let mut large_event_num = 0;
    // let delta = precise_time_s() - start_time;

    for (i, entry) in (1..=params.num_events).zip(
        WalkDir::new(format!("{}/BsPhiPhi/json/", manifest))
            .into_iter()
            .filter_map(|e| e.ok())
            .skip(1),
    ) {
        println!("{} {} {} {}", "Event".blue().red(), i, "out of".blue().bold(), params.num_events);
        match run_event(entry.path(), params.clone(), Some(track_dir.clone().into())) {
            Ok((oct_stats, full_stats)) => {
                total_time += full_stats.time;
                let mut file = OpenOptions::new()
                    .write(true)
                    .truncate(true)
                    .create(true)
                    .open(format!("{}/stats_{:04}.json", stat_dir, &full_stats.event_number))?;
                let all_stats = AllStats::new(oct_stats, full_stats);
                let serialized = serde_json::to_string(&all_stats).unwrap();
                write!(file, "{}", serialized)?;
                counter += 1;
            }
            Err(e) => {
                let message: String = format!("{}", e);
                if message.contains("Event size") {
                    large_event_num += 1;
                }
                println!("Full tracks are not built. {}", message);
                continue;
            }
        };
    }
    // let delta = precise_time_s() - start_time;
    println!("Number of events with size larger than threshold : {}", large_event_num);
    println!("All events took : {:>5.4}s", total_time);
    println!("Number of processed events : {:>5.4}", counter);
    Ok(())
}

fn run_event(
    file_path: &std::path::Path,
    opt_params: Parameters,
    tracks_dir: Option<PathBuf>,
) -> Result<(OctStats, FullTrackStats), Box<dyn Error>> {
    let start_time = precise_time_s();
    let (run_number, event_number) = match file_path.to_str() {
        Some(str_path) => {
            let relevant_parts: Vec<_> = str_path.rsplit('_').take(2).collect();
            let event_num = relevant_parts[0].split('.').next().unwrap();
            (relevant_parts[1], event_num)
        }
        None => {
            return Err(Box::new(EventRunError {
                message: "Couldn't parse file path into string".to_owned(),
            }))
        }
    };
    let mut all_stat_oct: OctStats = OctStats::default();
    let mut all_stat_full: FullTrackStats =
        FullTrackStats::from_event_number(event_number.parse().unwrap());
    let mut all_full_tracks = Vec::new();

    let mut data = EventData::from_file(&file_path, 0, &opt_params)?;
    println!("Full event size {}", data.FT.hits.len());
    for hit_zone in 0..2 {
        data.hit_zone = hit_zone;
        if data.FT.hits.len() > data.params.event_size_cut {
            return Err(Box::new(EventRunError {
                message: format!(
                    "Event size is larger than set threshold of {}",
                    data.params.event_size_cut
                ),
            }));
        }

        let mut algo_time = 0.0;
        let (quadruplets_1, t1) = timing!(data.par_find_quadruplets(0)?);
        let (quadruplets_2, t2) = timing!(data.par_find_quadruplets(1)?);
        let (quadruplets_3, t3) = timing!(data.par_find_quadruplets(2)?);
        algo_time = algo_time + t1 + t2 + t3;

        let (octuplets_23, t_oct) = timing!(data.get_octuplets(&quadruplets_3, &quadruplets_2)?);
        algo_time += t_oct;
        let oct_time = algo_time;

        let (full_tracks, t_full) = timing!(data.get_full_tracks(&octuplets_23, &quadruplets_1)?);
        algo_time += t_full;

        let (full_tracks, t_comp) = run_competitions(&data, &full_tracks);
        algo_time += t_comp;
        let stat_23 = OctStats::new(&data, &octuplets_23, oct_time);
        let stat_full =
            FullTrackStats::new(&data, &full_tracks, (t1, t2, t3, t_oct, t_full, algo_time));
        all_stat_oct.append(stat_23);
        all_stat_full.append(stat_full);
        all_full_tracks.append(&mut full_tracks.iter().map(|track| track.get_hit_ids()).collect());
    }
    // println!("{}", all_stat_oct);
    // println!("{}", all_stat_full);
    data.remove_found_hits(&all_full_tracks.iter().flatten().collect::<Vec<_>>());
    println!("Full event size after search {}", data.FT.hits.len());
    let serialized = serde_json::to_string(&all_full_tracks).unwrap();
    let mut file = match tracks_dir {
        Some(res_dir) => {
            OpenOptions::new().write(true).truncate(true).create(true).open(format!(
                "{}/{}_{:04}.json",
                res_dir.to_str().unwrap().to_string(),
                run_number,
                event_number
            ))?
        }
        None => OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .open(format!("found_tracks/{}_{:04}.json", run_number, event_number))?,
    };
    write!(file, "{}", serialized)?;

    // let delta = precise_time_s() - start_time;
    // println!(
    //     "{} {} took : {:>5.4}ms",
    //     "Entire run for event".bold().magenta(),
    //     event_number,
    //     delta * 1e3
    // );
    Ok((all_stat_oct, all_stat_full))
}

fn main() {
    let (file_name, opt_dir, params) = Opt::get_input();
    // if let Some(ref dir_name) = opt_dir {
    //     match fs::create_dir(dir_name) {
    //         Ok(_) => {}
    //         Err(e) => {
    //             println!("Couldn't create folder {:?}, exiting the program.", dir_name);
    //             println!("{}: {}", "Error".bold().red(), e);
    //             std::process::exit(1);
    //         }
    //     }
    // }
    if let Some(file_name) = file_name {
        match run_event(&file_name, params, opt_dir) {
            Ok((_, _)) => {}
            Err(e) => println!("Full tracks are not built. {}", e),
        }
    } else {
        match run_all(params, opt_dir) {
            Ok(()) => {}
            Err(e) => panic!("I/O error, couldn't write to file: {:?}", e),
        }
    }
}

fn run_competitions<'a>(
    data: &'a EventData,
    full_tracks: &'a [FullTrack],
) -> (Vec<FullTrack<'a>>, f64) {
    let mut comp_time = 0.0;
    // let (full_tracks, t_comp) = if data.params.full_xx_competition.0 && !full_tracks.is_empty() {
    //     timing!(track_competition(
    //         &full_tracks,
    //         data.params.full_xx_competition.1,
    //         Box::new(x_hits_in_common),
    //         Box::new(find_single_winner)
    //     ))
    // } else {
    //     (full_tracks.into(), 0.0)
    // };
    // comp_time += t_comp;
    let (full_tracks, t_comp) = if data.params.full_competition.0 && !full_tracks.is_empty() {
        timing!(track_competition(
            &full_tracks,
            1,
            Box::new(x_hits_in_common),
            Box::new(find_single_winner)
        ))
    } else {
        (full_tracks.into(), 0.0)
    };
    comp_time += t_comp;
    let (full_tracks, t_comp) = if data.params.full_competition.0 && !full_tracks.is_empty() {
        timing!(track_competition(
            &full_tracks,
            1,
            Box::new(x_hits_in_common),
            Box::new(find_single_winner)
        ))
    } else {
        (full_tracks.into(), 0.0)
    };
    comp_time += t_comp;

    (full_tracks, comp_time)
}

fn print_track_info<T: TrackHits>(full_tracks: &[T]) {
    let mut i = 1;
    println!("Print out track info");
    for track in full_tracks.iter() {
        print!("{} ", i);
        i += 1;
        for hit in track.hits().iter() {
            print!("{} ", hit.hit_id);
        }
        print!("{:>6} ", track.true_track());
        print!("{:0.2} ", track.chi2_x());
        print!("{:0.2} ", track.chi2_y());
        print!("{:0.2}", track.chi2_x() + track.chi2_y());
        println!();
        for hit in track.hits().iter() {
            print!("{:>10} ", hit.mcp_key);
        }
        println!();
    }
}
