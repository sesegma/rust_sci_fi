// #![feature(is_sorted)]
#[macro_use]

extern crate serde_derive;
pub mod args;
pub mod competition;
mod constants;
pub mod data_handler;
