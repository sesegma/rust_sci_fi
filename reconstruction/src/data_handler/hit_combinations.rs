use crate::constants::*;
use crate::data_handler::{FTHit, KeyType, Precision};
use nalgebra::{Matrix3, Vector3};
use ndarray::arr1;
use serde::ser::{Serialize, SerializeSeq, Serializer};
use std::collections::HashSet;
use std::hash::{Hash, Hasher};

#[derive(Debug, Clone, Default)]
pub struct TrackParameters {
    pub x_intercept: Precision,
    pub x_slope: Precision,
    pub x_quadratic: Precision,
    pub y_intercept: Precision,
    pub y_slope: Precision,
}

#[derive(Debug, Clone)]
pub struct Pair<'a> {
    pub hit1: &'a FTHit,
    pub hit4: &'a FTHit,
    pub tx: Precision,
    pub chi2_x: Precision,
}

impl<'a> Pair<'a> {
    pub fn new(hit1: &'a FTHit, hit4: &'a FTHit, tx: Precision, chi2_x: Precision) -> Self {
        Self {
            hit1,
            hit4,
            tx,
            chi2_x,
        }
    }
}

pub trait TrackHits: Clone + Eq + Hash {
    fn hits(&self) -> Vec<&FTHit>;
    fn hits_in_common(&self, other: &Self) -> Vec<u32>;
    fn x_hits_in_common(&self, other: &Self) -> Vec<u8>;
    fn chi2_x(&self) -> Precision;
    fn chi2_y(&self) -> Precision;
    fn true_track(&self) -> bool;
}

fn eval_parametrization_y(track_params: &TrackParameters, z: Precision) -> Precision {
    let dz = z - Z_REFERENCE;
    track_params.y_intercept + track_params.y_slope * dz
}

pub fn eval_parametrization_x(track_params: &TrackParameters, z: Precision) -> Precision {
    let dz = z - Z_REFERENCE;
    track_params.x_intercept + (track_params.x_slope + track_params.x_quadratic * dz) * dz
}

fn track_to_hit_distance(
    track_params: &TrackParameters,
    hit_x: Precision,
    hit_z: Precision,
    hit_dxdy: Precision,
) -> Precision {
    let z_hit = hit_z + DZDY * eval_parametrization_y(&track_params, hit_z);
    let x_track = eval_parametrization_x(track_params, z_hit);
    let y_track = eval_parametrization_y(track_params, z_hit);
    hit_x + y_track * hit_dxdy - x_track
}

fn get_z_estimate(hits: &[&FTHit], track_params: &TrackParameters) -> Vec<Precision> {
    hits.iter()
        .map(|hit| hit.z0 + DZDY * eval_parametrization_y(&track_params, hit.z0) - Z_REFERENCE)
        .collect()
}

fn get_z(hits: &[&FTHit]) -> Vec<Precision> {
    hits.iter().map(|hit| hit.z0 - Z_REFERENCE).collect()
}

pub fn calculate_linear_coeff(
    hits: &[&FTHit],
    hits_y: &[Precision],
    estimate_z: bool,
    track_params: &TrackParameters,
) -> (Precision, Precision) {
    let z_estimates = if estimate_z {
        get_z_estimate(&hits, &track_params)
    } else {
        get_z(&hits)
    };
    let hits_w: Vec<_> = hits.iter().map(|hit| (hit.w.sqrt())).collect();

    let Ay = hits_w.iter().fold(0.0, |acc, w| acc + w);
    let By = hits_w.iter().zip(z_estimates.iter()).fold(0.0, |acc, (w, z_est)| acc + (w * z_est));
    let Cy = hits_w
        .iter()
        .zip(z_estimates.iter())
        .fold(0.0, |acc, (w, z_est)| acc + (w * z_est * z_est));
    // calculate the right-hand side
    let Dy = hits_w.iter().zip(hits_y.iter()).fold(0.0, |acc, (w, x)| acc + w * x);
    let Ey = hits_w
        .iter()
        .zip(hits_y.iter())
        .zip(z_estimates.iter())
        .fold(0.0, |acc, ((w, x), z_est)| acc + w * z_est * x);

    // solve the system for the parameters of the quadratic equation.
    let y_slope = (Ay * Ey - By * Dy) / (Ay * Cy - By * By);
    let y_intercept = (Dy - By * y_slope) / Ay;

    (y_slope, y_intercept)
}

pub fn calculate_quad_coeff_with_linalg(
    hits: &[&FTHit],
    hits_x: &[Precision],
    estimate_z: bool,
    track_params: &TrackParameters,
) -> (Precision, Precision, Precision) {
    let z_estimates = if estimate_z {
        get_z_estimate(&hits, &track_params)
    } else {
        get_z(&hits)
    };
    // let hits_w: Vec<_> = hits.iter().map(|hit| 1.0 / (hit.w * hit.w)).collect();
    let hits_w: Vec<_> = hits.iter().map(|hit| (1.0 / hit.w.sqrt())).collect();
    let Ax = hits_w.iter().fold(0.0, |acc, w| acc + w);
    let Bx = hits_w.iter().zip(z_estimates.iter()).fold(0.0, |acc, (w, z_est)| acc + (w * z_est));
    let Cx = hits_w
        .iter()
        .zip(z_estimates.iter())
        .fold(0.0, |acc, (w, z_est)| acc + (w * z_est * z_est));
    let Dx = hits_w
        .iter()
        .zip(z_estimates.iter())
        .fold(0.0, |acc, (w, z_est)| acc + (w * z_est * z_est * z_est));
    let Ex = hits_w
        .iter()
        .zip(z_estimates.iter())
        .fold(0.0, |acc, (w, z_est)| acc + (w * z_est * z_est * z_est * z_est));

    // calculate the right-hand side
    let Fx = hits_w.iter().zip(hits_x.iter()).fold(0.0, |acc, (w, x)| acc + w * x);
    let Gx = hits_w
        .iter()
        .zip(hits_x.iter())
        .zip(z_estimates.iter())
        .fold(0.0, |acc, ((w, x), z_est)| acc + w * z_est * x);
    let Hx = hits_w
        .iter()
        .zip(hits_x.iter())
        .zip(z_estimates.iter())
        .fold(0.0, |acc, ((w, x), z_est)| acc + w * z_est * z_est * x);

    // solve the system for the parameters of the quadratic equation.

    let A = Matrix3::new(Ax, Bx, Cx, Bx, Cx, Dx, Cx, Dx, Ex);
    let b = Vector3::new(Fx, Gx, Hx);
    let x = A.lu().solve(&b).unwrap();

    (x[0], x[1], x[2])
}

fn chi2(
    tot_hits_x: &[&FTHit],
    hits_x: &[Precision],
    tot_hits_y: &[&FTHit],
    hits_y: &[Precision],
    track_params: &TrackParameters,
    ndof_x: usize,
    ndof_y: usize,
) -> (Precision, Precision) {
    let z_x: Vec<Precision> = get_z_estimate(tot_hits_x, &track_params);
    let chi2_x =
        hits_x.iter().zip(z_x.iter()).zip(tot_hits_x.iter()).fold(0.0, |acc, ((x, z), hit)| {
            acc + ((x
                - (track_params.x_intercept
                    + (track_params.x_slope + track_params.x_quadratic * z) * z))
                .powi(2))
                * hit.w
        }) / ndof_x as Precision;

    let z_y = get_z_estimate(tot_hits_y, &track_params);
    let chi2_y =
        hits_y.iter().zip(z_y.iter()).zip(tot_hits_y.iter()).fold(0.0, |acc, ((y, z), hit)| {
            acc + ((y - (track_params.y_intercept + track_params.y_slope * z)).powi(2)) * hit.w
        }) / ndof_y as Precision;
    (chi2_x, chi2_y)
}

/// Struct includes four hits from the same station, along with values calcuated
/// in the process of building it.
#[derive(Debug, Clone)]
pub struct Quadruplet<'a> {
    pub hit4: &'a FTHit,
    pub hit3: &'a FTHit,
    pub hit2: &'a FTHit,
    pub hit1: &'a FTHit,
    pub x_u: Precision,
    pub x_v: Precision,
    pub tx: Precision,
    pub ty: Precision,
    pub y_u: Precision,
    pub y_v: Precision,
    // pub z_u: Precision,
    // pub z_v: Precision,
}

#[derive(Debug, Clone)]
pub struct OctData {
    proj_x_parabolic: Precision,
    proj_x_linear: Precision,
}

#[derive(Debug, Clone)]
pub struct Octuplet<'a> {
    pub quad_down: &'a Quadruplet<'a>,
    pub quad_up: &'a Quadruplet<'a>,
    pub chi2_x: Precision,
    pub chi2_y: Precision,
    pub track_params: TrackParameters,
    pub track_params_linear: TrackParameters,
    pub true_track: bool,
    pub majority_key: Option<KeyType>,
}

impl<'a> TrackHits for Octuplet<'a> {
    fn hits(&self) -> Vec<&FTHit> {
        vec![
            self.quad_up.hit1,
            self.quad_up.hit2,
            self.quad_up.hit3,
            self.quad_up.hit4,
            self.quad_down.hit1,
            self.quad_down.hit2,
            self.quad_down.hit3,
            self.quad_down.hit4,
        ]
    }

    fn hits_in_common(&self, other: &Octuplet) -> Vec<u32> {
        let own_ids = self.hits().iter().map(|hit| hit.hit_id).collect::<HashSet<u32>>();
        let other_ids = other.hits().iter().map(|hit| hit.hit_id).collect::<HashSet<u32>>();
        own_ids.intersection(&other_ids).cloned().into_iter().collect()
    }

    fn x_hits_in_common(&self, other: &Octuplet) -> Vec<u8> {
        let x_layer = [0, 3, 4, 7, 8, 11];
        let own_ids = self
            .hits()
            .iter()
            .filter_map(|hit| {
                if x_layer.contains(&hit.layer) {
                    Some(hit.layer)
                } else {
                    None
                }
            })
            .collect::<HashSet<u8>>();
        let other_ids = other
            .hits()
            .iter()
            .filter_map(|hit| {
                if x_layer.contains(&hit.layer) {
                    Some(hit.layer)
                } else {
                    None
                }
            })
            .collect::<HashSet<u8>>();
        own_ids.intersection(&other_ids).cloned().into_iter().collect()
    }

    fn chi2_x(&self) -> Precision {
        self.chi2_x
    }

    fn chi2_y(&self) -> Precision {
        self.chi2_y
    }

    fn true_track(&self) -> bool {
        self.true_track
    }
}

/// This impl is required for Hash trait to work.
impl<'a> PartialEq for Octuplet<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.quad_up.hit1 == other.quad_up.hit1
            && self.quad_up.hit2 == other.quad_up.hit2
            && self.quad_up.hit3 == other.quad_up.hit3
            && self.quad_up.hit4 == other.quad_up.hit4
            && self.quad_down.hit1 == other.quad_down.hit1
            && self.quad_down.hit2 == other.quad_down.hit2
            && self.quad_down.hit3 == other.quad_down.hit3
            && self.quad_down.hit4 == other.quad_down.hit4
    }
}
impl<'a> Eq for Octuplet<'a> {}

impl<'a> Hash for Octuplet<'a> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hits().hash(state);
    }
}
impl<'a> Octuplet<'a> {
    /// Calculates chi-squared for the linear fits for x and y coordinate separately.
    pub fn chi2_linear(
        quad_up: &Quadruplet,
        quad_down: &Quadruplet,
        ndof_x: usize,
        ndof_y: usize,
    ) -> (Precision, Precision, TrackParameters) {
        let weights_x = 1.0
            / arr1(&[
                quad_up.hit1.w.sqrt(),
                quad_up.hit2.w.sqrt(),
                quad_up.hit3.w.sqrt(),
                quad_up.hit4.w.sqrt(),
                quad_down.hit1.w.sqrt(),
                quad_down.hit2.w.sqrt(),
                quad_down.hit3.w.sqrt(),
                quad_down.hit4.w.sqrt(),
            ]);
        // let weights_x = 1.0 / weights_x.mapv(|elem| elem.powi(2));
        let z_x = arr1(&[
            quad_up.hit1.z0,
            quad_up.hit2.z0,
            quad_up.hit3.z0,
            quad_up.hit4.z0,
            quad_down.hit1.z0,
            quad_down.hit2.z0,
            quad_down.hit3.z0,
            quad_down.hit4.z0,
        ]) - Z_REFERENCE;
        let x = arr1(&[
            quad_up.hit1.x0,
            quad_up.x_u,
            quad_up.x_v,
            quad_up.hit4.x0,
            quad_down.hit1.x0,
            quad_down.x_u,
            quad_down.x_v,
            quad_down.hit4.x0,
        ]);
        let A_x: Precision = weights_x.sum();
        let B_x: Precision = weights_x.dot(&z_x);
        let C_x: Precision = (&weights_x * &z_x * &z_x).sum();
        let D_x: Precision = weights_x.dot(&x);
        let E_x: Precision = (&weights_x * &z_x * &x).sum();

        let x_slope = (A_x * E_x - B_x * D_x) / (A_x * C_x - B_x * B_x);
        let x_intercept = (D_x - B_x * x_slope) / A_x;

        // let weights_y = 1.0
        //     / (arr1(&[quad_up.hit2.w, quad_up.hit3.w, quad_down.hit2.w, quad_down.hit3.w]))
        //         .mapv(|elem| elem.powi(2));
        let weights_y = 1.0
            / arr1(&[
                quad_up.hit2.w.sqrt(),
                quad_up.hit3.w.sqrt(),
                quad_down.hit2.w.sqrt(),
                quad_down.hit3.w.sqrt(),
            ]);
        let z_y = arr1(&[quad_up.hit2.z0, quad_up.hit3.z0, quad_down.hit2.z0, quad_down.hit3.z0])
            - Z_REFERENCE;
        let y = arr1(&[quad_up.y_u, quad_up.y_v, quad_down.y_u, quad_down.y_v]);

        let A_y: Precision = weights_y.sum();
        let B_y: Precision = weights_y.dot(&z_y);
        let C_y: Precision = (&weights_y * &z_y * &z_y).sum();
        let D_y: Precision = weights_y.dot(&y);
        let E_y: Precision = (&weights_y * &z_y * &y).sum();

        let y_slope = (A_y * E_y - B_y * D_y) / (A_y * C_y - B_y * B_y);
        let y_intercept = (D_y - B_y * y_slope) / A_y;
        let w_x = arr1(&[
            quad_up.hit1.w,
            quad_up.hit2.w,
            quad_up.hit3.w,
            quad_up.hit4.w,
            quad_down.hit1.w,
            quad_down.hit2.w,
            quad_down.hit3.w,
            quad_down.hit4.w,
        ]);
        let w_y = arr1(&[quad_up.hit2.w, quad_up.hit3.w, quad_down.hit2.w, quad_down.hit3.w]);

        let chi2_x = ((x - (x_intercept + x_slope * &z_x)).mapv(|elem| elem.powi(2)) * w_x).sum();
        let chi2_y = ((y - (y_intercept + y_slope * &z_y)).mapv(|elem| elem.powi(2)) * w_y).sum();
        (
            chi2_x / ndof_x as Precision,
            chi2_y / ndof_y as Precision,
            TrackParameters {
                x_intercept,
                x_slope,
                y_intercept,
                y_slope,
                x_quadratic: 0.0,
            },
        )
    }
    pub fn chi2_parabolic(
        quad_1: &Quadruplet,
        quad_2: &Quadruplet,
        ndof_x: usize,
        ndof_y: usize,
    ) -> (Precision, Precision, TrackParameters) {
        let tot_hits_x = [
            quad_1.hit1,
            quad_1.hit2,
            quad_1.hit3,
            quad_1.hit4,
            quad_2.hit1,
            quad_2.hit2,
            quad_2.hit3,
            quad_2.hit4,
        ];
        let hits_x = [
            quad_1.hit1.x0,
            quad_1.x_u,
            quad_1.x_v,
            quad_1.hit4.x0,
            quad_2.hit1.x0,
            quad_2.x_u,
            quad_2.x_v,
            quad_2.hit4.x0,
        ];
        let hits_y = [quad_1.y_u, quad_1.y_v, quad_2.y_u, quad_2.y_v];
        let tot_hits_y = [quad_1.hit2, quad_1.hit3, quad_2.hit2, quad_2.hit3];
        let mut track_params = TrackParameters::default();

        let (a, b, c) =
            calculate_quad_coeff_with_linalg(&tot_hits_x, &hits_x, false, &track_params);
        track_params.x_intercept = a;
        track_params.x_slope = b;
        track_params.x_quadratic = c;
        let (d, e) = calculate_linear_coeff(&tot_hits_y, &hits_y, false, &track_params);
        track_params.y_slope = d;
        track_params.y_intercept = e;
        let (chi2_x, chi2_y) =
            chi2(&tot_hits_x, &hits_x, &tot_hits_y, &hits_y, &track_params, ndof_x, ndof_y);
        (chi2_x, chi2_y, track_params)
    }
}

#[derive(Debug, Clone)]
pub struct FullTrack<'a> {
    pub quad_1: &'a Quadruplet<'a>,
    pub quad_2: &'a Quadruplet<'a>,
    pub quad_3: &'a Quadruplet<'a>,
    pub track_params: TrackParameters,
    pub chi2_x: Precision,
    pub chi2_y: Precision,
    pub true_track: bool,
    pub majority_key: Option<KeyType>,
    pub x_proj_parabolic: Precision,
    pub x_proj_linear: Precision,
    pub tx_parabolic: Precision,
    pub tx_linear: Precision,
}

impl<'a> TrackHits for FullTrack<'a> {
    fn hits(&self) -> Vec<&FTHit> {
        vec![
            self.quad_1.hit1,
            self.quad_1.hit2,
            self.quad_1.hit3,
            self.quad_1.hit4,
            self.quad_2.hit1,
            self.quad_2.hit2,
            self.quad_2.hit3,
            self.quad_2.hit4,
            self.quad_3.hit1,
            self.quad_3.hit2,
            self.quad_3.hit3,
            self.quad_3.hit4,
        ]
    }
    fn hits_in_common(&self, other: &FullTrack) -> Vec<u32> {
        let own_ids = self.hits().iter().map(|hit| hit.hit_id).collect::<HashSet<u32>>();
        let other_ids = other.hits().iter().map(|hit| hit.hit_id).collect::<HashSet<u32>>();
        own_ids.intersection(&other_ids).cloned().into_iter().collect()
    }
    fn x_hits_in_common(&self, other: &FullTrack) -> Vec<u8> {
        let x_layer = [0, 3, 4, 7, 8, 11];
        let own_ids = self
            .hits()
            .iter()
            .filter_map(|hit| {
                if x_layer.contains(&hit.layer) {
                    Some(hit.layer)
                } else {
                    None
                }
            })
            .collect::<HashSet<u8>>();
        let other_ids = other
            .hits()
            .iter()
            .filter_map(|hit| {
                if x_layer.contains(&hit.layer) {
                    Some(hit.layer)
                } else {
                    None
                }
            })
            .collect::<HashSet<u8>>();
        own_ids.intersection(&other_ids).cloned().into_iter().collect()
    }
    fn chi2_x(&self) -> Precision {
        self.chi2_x
    }
    fn chi2_y(&self) -> Precision {
        self.chi2_y
    }
    fn true_track(&self) -> bool {
        self.true_track
    }
}

impl<'a> FullTrack<'a> {
    /// Fits a parabola to the x-measurements of a full track, and a linear track
    /// to calculated y-measurements. Considers all 12 x-coordinates, it means that it
    /// includes extrapolated values for all uv-layers.
    pub fn fit_parabola(quad_1: &Quadruplet, oct: &Octuplet) -> TrackParameters {
        // get the projected z-coordinates
        let hits = [
            quad_1.hit1,
            quad_1.hit2,
            quad_1.hit3,
            quad_1.hit4,
            oct.quad_up.hit1,
            oct.quad_up.hit2,
            oct.quad_up.hit3,
            oct.quad_up.hit4,
            oct.quad_down.hit1,
            oct.quad_down.hit2,
            oct.quad_down.hit3,
            oct.quad_down.hit4,
        ];
        let hits_x = [
            quad_1.hit1.x0,
            quad_1.x_u,
            quad_1.x_v,
            quad_1.hit4.x0,
            oct.quad_up.hit1.x0,
            oct.quad_up.x_u,
            oct.quad_up.x_v,
            oct.quad_up.hit4.x0,
            oct.quad_down.hit1.x0,
            oct.quad_down.x_u,
            oct.quad_down.x_v,
            oct.quad_down.hit4.x0,
        ];
        let hits_y = [
            quad_1.y_u,
            quad_1.y_v,
            oct.quad_up.y_u,
            oct.quad_up.y_v,
            oct.quad_down.y_u,
            oct.quad_down.y_v,
        ];
        let tot_hits_y = [
            quad_1.hit2,
            quad_1.hit3,
            oct.quad_up.hit2,
            oct.quad_up.hit3,
            oct.quad_down.hit2,
            oct.quad_down.hit3,
        ];

        // calculate the entries in the squared Vandermonde matrix - left-hand side
        let mut track_params = TrackParameters {
            x_intercept: oct.track_params.x_intercept,
            x_slope: oct.track_params.x_slope,
            x_quadratic: oct.track_params.x_quadratic, /* initialize to something, this will get
                                                        * changes anyway */
            y_intercept: oct.track_params.y_intercept,
            y_slope: oct.track_params.y_slope,
        };

        let (a, b, c) = calculate_quad_coeff_with_linalg(&hits, &hits_x, true, &track_params);
        track_params.x_intercept = a;
        track_params.x_slope = b;
        track_params.x_quadratic = c;
        let (d, e) = calculate_linear_coeff(&tot_hits_y, &hits_y, true, &track_params);
        track_params.y_slope = d;
        track_params.y_intercept = e;
        track_params
    }
    // calculate the chi-squared of the track in xz and yz planes
    pub fn chi2_parabolic(
        track_params: &TrackParameters,
        quad_1: &Quadruplet,
        oct: &Octuplet,
        ndof_x: usize,
        ndof_y: usize,
    ) -> (Precision, Precision) {
        let tot_hits_x = [
            quad_1.hit1,
            quad_1.hit2,
            quad_1.hit3,
            quad_1.hit4,
            oct.quad_up.hit1,
            oct.quad_up.hit2,
            oct.quad_up.hit3,
            oct.quad_up.hit4,
            oct.quad_down.hit1,
            oct.quad_down.hit2,
            oct.quad_down.hit3,
            oct.quad_down.hit4,
        ];
        let hits_x = [
            quad_1.hit1.x0,
            quad_1.x_u,
            quad_1.x_v,
            quad_1.hit4.x0,
            oct.quad_up.hit1.x0,
            oct.quad_up.x_u,
            oct.quad_up.x_v,
            oct.quad_up.hit4.x0,
            oct.quad_down.hit1.x0,
            oct.quad_down.x_u,
            oct.quad_down.x_v,
            oct.quad_down.hit4.x0,
        ];
        let hits_y = [
            quad_1.y_u,
            quad_1.y_v,
            oct.quad_up.y_u,
            oct.quad_up.y_v,
            oct.quad_down.y_u,
            oct.quad_down.y_v,
        ];
        let tot_hits_y = [
            quad_1.hit2,
            quad_1.hit3,
            oct.quad_up.hit2,
            oct.quad_up.hit3,
            oct.quad_down.hit2,
            oct.quad_down.hit3,
        ];

        chi2(&tot_hits_x, &hits_x, &tot_hits_y, &hits_y, &track_params, ndof_x, ndof_y)
    }

    pub fn get_vec_of_xw(&self) -> Vec<(Precision, Precision, Precision)> {
        vec![
            (self.quad_1.hit1.z0, self.quad_1.hit1.x0, self.quad_1.hit1.w),
            (self.quad_1.hit2.z0, self.quad_1.x_u, self.quad_1.hit2.w),
            (self.quad_1.hit3.z0, self.quad_1.x_v, self.quad_1.hit3.w),
            (self.quad_1.hit4.z0, self.quad_1.hit4.x0, self.quad_1.hit4.w),
            (self.quad_2.hit1.z0, self.quad_2.hit1.x0, self.quad_2.hit1.w),
            (self.quad_2.hit2.z0, self.quad_2.x_u, self.quad_2.hit2.w),
            (self.quad_2.hit3.z0, self.quad_2.x_v, self.quad_2.hit3.w),
            (self.quad_2.hit4.z0, self.quad_2.hit4.x0, self.quad_2.hit4.w),
            (self.quad_3.hit1.z0, self.quad_3.hit1.x0, self.quad_3.hit1.w),
            (self.quad_3.hit2.z0, self.quad_3.x_u, self.quad_3.hit2.w),
            (self.quad_3.hit3.z0, self.quad_3.x_v, self.quad_3.hit3.w),
            (self.quad_3.hit4.z0, self.quad_3.hit4.x0, self.quad_3.hit4.w),
        ]
    }

    pub fn get_vec_of_xw_xlayers(&self) -> Vec<(Precision, Precision, Precision)> {
        vec![
            (self.quad_1.hit1.z0, self.quad_1.hit1.x0, self.quad_1.hit1.w),
            (self.quad_1.hit4.z0, self.quad_1.hit4.x0, self.quad_1.hit4.w),
            (self.quad_2.hit1.z0, self.quad_2.hit1.x0, self.quad_2.hit1.w),
            (self.quad_2.hit4.z0, self.quad_2.hit4.x0, self.quad_2.hit4.w),
            (self.quad_3.hit1.z0, self.quad_3.hit1.x0, self.quad_3.hit1.w),
            (self.quad_3.hit4.z0, self.quad_3.hit4.x0, self.quad_3.hit4.w),
        ]
    }
    pub fn get_vec_of_yw(&self) -> Vec<(Precision, Precision)> {
        vec![
            (self.quad_1.y_u, self.quad_1.hit2.w),
            (self.quad_1.y_v, self.quad_1.hit3.w),
            (self.quad_2.y_u, self.quad_2.hit2.w),
            (self.quad_2.y_v, self.quad_2.hit3.w),
            (self.quad_3.y_u, self.quad_3.hit2.w),
            (self.quad_3.y_v, self.quad_3.hit3.w),
        ]
    }

    pub fn get_hit_ids(&self) -> Vec<u32> {
        let mut hit_ids = Vec::new();
        hit_ids.push(self.quad_1.hit1.hit_id);
        hit_ids.push(self.quad_1.hit2.hit_id);
        hit_ids.push(self.quad_1.hit3.hit_id);
        hit_ids.push(self.quad_1.hit4.hit_id);
        hit_ids.push(self.quad_2.hit1.hit_id);
        hit_ids.push(self.quad_2.hit2.hit_id);
        hit_ids.push(self.quad_2.hit3.hit_id);
        hit_ids.push(self.quad_2.hit4.hit_id);
        hit_ids.push(self.quad_3.hit1.hit_id);
        hit_ids.push(self.quad_3.hit2.hit_id);
        hit_ids.push(self.quad_3.hit3.hit_id);
        hit_ids.push(self.quad_3.hit4.hit_id);
        hit_ids
    }
}

impl<'a> PartialEq for FullTrack<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.quad_1.hit1 == other.quad_1.hit1
            && self.quad_1.hit2 == other.quad_1.hit2
            && self.quad_1.hit3 == other.quad_1.hit3
            && self.quad_1.hit4 == other.quad_1.hit4
            && self.quad_2.hit1 == other.quad_2.hit1
            && self.quad_2.hit2 == other.quad_2.hit2
            && self.quad_2.hit3 == other.quad_2.hit3
            && self.quad_2.hit4 == other.quad_2.hit4
            && self.quad_3.hit1 == other.quad_3.hit1
            && self.quad_3.hit2 == other.quad_3.hit2
            && self.quad_3.hit3 == other.quad_3.hit3
            && self.quad_3.hit4 == other.quad_3.hit4
    }
}
impl<'a> Eq for FullTrack<'a> {}

impl<'a> Hash for FullTrack<'a> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hits().hash(state);
    }
}

impl<'a> Serialize for FullTrack<'a> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut seq = serializer.serialize_seq(Some(12))?;
        seq.serialize_element(&self.quad_1.hit1.hit_id)?;
        seq.serialize_element(&self.quad_1.hit2.hit_id)?;
        seq.serialize_element(&self.quad_1.hit3.hit_id)?;
        seq.serialize_element(&self.quad_1.hit4.hit_id)?;
        seq.serialize_element(&self.quad_2.hit1.hit_id)?;
        seq.serialize_element(&self.quad_2.hit2.hit_id)?;
        seq.serialize_element(&self.quad_2.hit3.hit_id)?;
        seq.serialize_element(&self.quad_2.hit4.hit_id)?;
        seq.serialize_element(&self.quad_3.hit1.hit_id)?;
        seq.serialize_element(&self.quad_3.hit2.hit_id)?;
        seq.serialize_element(&self.quad_3.hit3.hit_id)?;
        seq.serialize_element(&self.quad_3.hit4.hit_id)?;
        seq.end()
    }
}
