use crate::data_handler::{FTHit, Precision};

/// Contains a hit from the first x-layer and the associated hits from the v-layer
/// along with the angle tx generated for each x-x pair
#[derive(Debug)]
pub struct PairedXHit<'a> {
    pub tx: Precision,
    pub hit: &'a FTHit,
    pub hits_v: Vec<VHit<'a>>,
}

impl<'a> PairedXHit<'a> {
    pub fn new(hit: &'a FTHit, tx: Precision, hits_v: Vec<VHit<'a>>) -> Self {
        Self {
            hit,
            tx,
            hits_v,
        }
    }
}

/// Contains a hit from the v-layer and the associated hits from the u-layer
/// along with the extrapolated x-coordinate from the v-layer.
#[derive(Debug)]
pub struct VHit<'a> {
    pub hit: &'a FTHit,
    pub x_v: Precision,
    pub hits_u: Vec<UHit<'a>>,
}

impl<'a> VHit<'a> {
    pub fn new(hit: &'a FTHit, x_v: Precision, hits_u: Vec<UHit<'a>>) -> Self {
        Self {
            hit,
            x_v,
            hits_u,
        }
    }
}

/// Contains a reference to a u-layer hit and the information from the Quadruplet
/// building process.
#[derive(Debug)]
pub struct UHit<'a> {
    pub hit: &'a FTHit,
    pub x_u: Precision,
    pub y_u: Precision,
    pub y_v: Precision,
    // pub z_u: Precision,
    // pub z_v: Precision,
    pub ty: Precision,
}

impl<'a> UHit<'a> {
    pub fn new(
        hit: &'a FTHit,
        x_u: Precision,
        y_u: Precision,
        y_v: Precision,
        // z_u: Precision,
        // z_v: Precision,
        ty: Precision,
    ) -> Self {
        Self {
            hit,
            x_u,
            y_u,
            y_v,
            // z_u,
            // z_v,
            ty,
        }
    }
}

/// Contains an original hit from last layer of a station
/// and a vector of hits from the first layer of the same station that were
/// selected as possibly belonging to the same track
#[derive(Debug)]
pub struct Xpair<'a> {
    pub orig_hit: &'a FTHit,
    pub pairs: Vec<PairedXHit<'a>>,
}

impl<'a> Xpair<'a> {
    pub fn new(orig_hit: &'a FTHit, pairs: Vec<PairedXHit<'a>>) -> Self {
        Self {
            orig_hit,
            pairs,
        }
    }
}
