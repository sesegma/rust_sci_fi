use crate::data_handler::Precision;
use colored::*;
use std::{default::Default, fs::File, io::BufReader, path::PathBuf};
use structopt::StructOpt;

#[structopt(name = "SciFi", about = "Choose parameters for SciFi track reconstruction")]
#[derive(Debug, StructOpt)]
pub struct Opt {
    #[structopt(short = "a", long = "all")]
    /// All files will be processed
    all: bool,
    #[structopt(short = "f", long = "file")]
    /// A file containing Monte Carlo simulated data
    file_name: Option<PathBuf>,
    // #[structopt(short = "e", long = "event")]
    // event_number: Option<u32>,
    // #[structopt(short = "h", long = "hitZone", default_value = "0")]
    // hit_zone: u8,
    #[structopt(short = "p", long = "parameters")]
    /// .yml file containing parameters for CA seeding
    param_file: Option<PathBuf>,
    #[structopt(short = "d", long = "target_directory")]
    /// name of a directory where all results will be saved
    target_directory: Option<PathBuf>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Parameters {
    pub event_size_cut: usize,
    pub segment_cut: Precision,
    pub windows_3: (Precision, Precision, Precision, Precision),
    pub windows_2: (Precision, Precision, Precision, Precision),
    pub windows_1: (Precision, Precision, Precision, Precision),
    pub stereo_windows: (Precision, Precision, Precision, Precision),
    pub y_u_windows: (Precision, Precision),
    pub x_interval_2: Precision,
    pub x_interval_1: Precision,
    pub tx_cut: Precision,
    pub ty_cut: Precision,
    pub chi2_x_cut_23: Precision,
    pub chi2_y_cut: Precision,
    pub chi2_x_cut_1: Precision,
    pub chi2_y_cut_1: Precision,
    pub y_limit_uv: (Precision, Precision, Precision, Precision, Precision, Precision),
    pub intercept_limit: (Precision, Precision, Precision),
    pub max_len_oct_tracks: usize,
    pub max_len_full_tracks: usize,
    pub oct_competition: (bool, usize),
    pub full_competition: (bool, usize),
    // pub oct_common_keys_len: usize,
    // pub full_common_keys_len: usize,
    pub full_xx_competition: (bool, usize),
    pub num_events: usize,
}

impl Default for Parameters {
    fn default() -> Self {
        Self {
            event_size_cut: 10000,
            segment_cut: 529.0,
            windows_2: (80., 130., 70., 240.),
            windows_3: (90., 130., 80., 240.),
            windows_1: (90., 130., 90., 240.),
            stereo_windows: (0.0, 150.0, 0.0, 200.0),
            y_u_windows: (25., 30.),
            x_interval_2: 10.0,
            x_interval_1: 15.0,
            tx_cut: 0.1,
            ty_cut: 0.3,
            chi2_x_cut_23: 800.,
            chi2_y_cut: 1200.,
            chi2_x_cut_1: 20.,
            chi2_y_cut_1: 800.,
            y_limit_uv: (2070.0, 2080.0, 2230.0, 2250.0, 2400.0, 2424.0),
            intercept_limit: (1000.0, 1100.0, 1200.0),
            max_len_oct_tracks: 80000,
            max_len_full_tracks: 80000,
            oct_competition: (false, 6),
            full_competition: (true, 8),
            full_xx_competition: (true, 2),
            num_events: 500,
        }
    }
}

impl Opt {
    pub fn get_input() -> (Option<PathBuf>, Option<PathBuf>, Parameters) {
        let opt = Self::from_args();
        // println!("{:?}", opt);

        let params = {
            if let Some(file) = opt.param_file {
                let reader =
                    BufReader::new(File::open(file).expect("Couldn't open parameter file"));
                match serde_yaml::from_reader(reader) {
                    Ok(p) => p,
                    Err(e) => {
                        println!(
                            "{}",
                            "Couldn't read provided parameter file, exiting the program."
                                .bold()
                                .magenta()
                        );
                        println!("{}: {}", "Error".bold().red(), e);
                        std::process::exit(1);
                    }
                }
            } else {
                Parameters::default()
            }
        };
        // if let Some(ref dir_name) = opt.target_directory {
        //      match fs::create_dir(dir_name) {
        //          Ok(_) => {}
        //          Err(e) => {
        //              println!("Couldn't create folder {:?}, exiting the program.", dir_name);
        //              println!("{}: {}", "Error".bold().red(), e);
        //              std::process::exit(1);
        //          }
        //      }
        //  }

        // let file_name = opt.file_name;
        if opt.all {
            (None, opt.target_directory, params)
        } else {
            (opt.file_name, opt.target_directory, params)
        }
    }
}
