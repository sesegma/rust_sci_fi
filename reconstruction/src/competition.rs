use crate::data_handler::*;
use std::collections::HashSet;

pub fn track_competition<'a, T, U, F>(
    tracks: &[T],
    common_keys_len: usize,
    pair_comparator: U,
    mut win_decider: F,
) -> Vec<T>
where
    T: TrackHits + std::fmt::Debug,
    F: FnMut(&mut HashSet<T>, &mut Vec<T>) + 'a,
    U: Fn(&T, &T) -> Vec<u32>,
{
    // println!("\n------------------Competition time!-----------------\n");
    let mut result = Vec::new();
    let mut cur_track = tracks[0].clone();
    let mut temp_group = HashSet::new();
    temp_group.insert(cur_track.clone());
    for track in tracks.iter().skip(1) {
        let common_keys = pair_comparator(&cur_track, track);
        // if common_keys.contains(&2684944418) {
        //     dbg!(&cur_track);
        // }
        let common_keys_length = common_keys.len();
        if common_keys_length >= common_keys_len {
            if temp_group.contains(&track) && track.true_track() {
                temp_group.replace(track.clone());
            } else {
                temp_group.insert(track.clone());
            }
        } else {
            //choose tracks from temp_group and add to result based on chi2
            win_decider(&mut temp_group, &mut result);
            // start a new temp_group
            // print_track_info(&temp_group.clone().into_iter().collect::<Vec<_>>());
            temp_group = HashSet::new();
            temp_group.insert(track.clone());
        }
        cur_track = track.clone();
    }
    win_decider(&mut temp_group, &mut result);
    // print_track_info(&temp_group.clone().iter().collect());
    result
}

pub fn grouped_track_competition<'a, T, F>(
    tracks: &[T],
    // common_keys_len: usize,
    // pair_comparator: U,
    mut win_decider: F,
) -> Vec<T>
where
    T: TrackHits,
    F: FnMut(&mut HashSet<T>, &mut Vec<T>) + 'a,
{
    // println!("\n------------------Competition time!-----------------\n");
    let mut result = Vec::new();
    let mut temp_group = HashSet::new();
    for track in tracks {
        temp_group.insert(track.clone());
    }
    // let mut temp_group = tracks.clone().into_iter().collect::<HashSet<T>>();
    win_decider(&mut temp_group, &mut result);
    // print_track_info(&temp_group.clone().iter().collect());
    result
}

pub fn find_winners<'a, T: TrackHits + 'a>(hit_group: &mut HashSet<T>, result_vec: &mut Vec<T>) {
    if hit_group.len() > 2 {
        let winner_1 = hit_group
            .iter()
            .min_by(|track1, track2| track1.chi2_x().partial_cmp(&track2.chi2_x()).unwrap())
            .unwrap();
        let winner_2 = hit_group
            .iter()
            .min_by(|track1, track2| track1.chi2_y().partial_cmp(&track2.chi2_y()).unwrap())
            .unwrap();
        let winner_3 = hit_group
            .iter()
            .min_by(|track1, track2| {
                // track1.chi2_x.partial_cmp(&track2.chi2_x).unwrap()
                (track1.chi2_x() + track1.chi2_y())
                    .partial_cmp(&(track2.chi2_x() + track2.chi2_y()))
                    .unwrap()
            })
            .unwrap();

        if winner_1 == winner_2 || winner_1 == winner_3 {
            result_vec.push(winner_1.clone());
        } else if winner_2 == winner_3 {
            result_vec.push(winner_2.clone());
        } else {
            result_vec.push(winner_1.clone());
            result_vec.push(winner_2.clone());
            result_vec.push(winner_3.clone());
        }
    } else {
        for track in hit_group.iter() {
            result_vec.push(track.clone());
        }
    }
}

pub fn find_winners_by_chi2_x<'a, T: TrackHits + 'a>(
    hit_group: &mut HashSet<T>,
    result_vec: &mut Vec<T>,
) {
    if hit_group.len() > 2 {
        let mut group_vec = hit_group.iter().collect::<Vec<_>>();
        group_vec.sort_by(|track1, track2| track1.chi2_x().partial_cmp(&track2.chi2_x()).unwrap());
        let winner_1 = group_vec.pop().unwrap();
        let winner_2 = group_vec.pop().unwrap();
        let winner_3 = group_vec.pop().unwrap();

        if winner_1 == winner_2 || winner_1 == winner_3 {
            result_vec.push(winner_1.clone());
        } else if winner_2 == winner_3 {
            result_vec.push(winner_2.clone());
        } else {
            result_vec.push(winner_1.clone());
            result_vec.push(winner_2.clone());
            result_vec.push(winner_3.clone());
        }
    } else {
        for track in hit_group.iter() {
            result_vec.push(track.clone());
        }
    }
}

pub fn find_single_winner<'a, T: TrackHits + 'a>(
    hit_group: &mut HashSet<T>,
    result_vec: &mut Vec<T>,
) {
    if hit_group.len() > 1 {
        let winner_1 = hit_group
            .iter()
            .min_by(|track1, track2| {
                track1.chi2_x().partial_cmp(&track2.chi2_x()).unwrap()
                // (track1.chi2_x() + track1.chi2_y())
                //     .partial_cmp(&(track2.chi2_x() + track2.chi2_y()))
                //     .unwrap()
            })
            .unwrap();

        result_vec.push(winner_1.clone());
    } else {
        for track in hit_group.iter() {
            result_vec.push(track.clone());
        }
    }
}

pub fn hits_in_common<T: TrackHits>(a: &T, b: &T) -> Vec<u32> {
    // let own_ids = a.hits().iter().map(|hit| hit.hit_id).collect::<Vec<u32>>();
    // let other_ids = b.hits().iter().map(|hit| hit.hit_id).collect::<Vec<u32>>();
    // own_ids.intersection(&other_ids).cloned().into_iter().collect();

    a.hits()
        .iter()
        .map(|hit| hit.hit_id)
        .zip(b.hits().iter().map(|hit| hit.hit_id))
        .filter_map(|(id1, id2)| {
            if id1 == id2 {
                Some(id1)
            } else {
                None
            }
        })
        .collect()
}

pub fn x_hits_in_common<T: TrackHits>(a: &T, b: &T) -> Vec<u32> {
    let x_layer = [0, 3, 4, 5, 8, 11];
    let own_ids = a
        .hits()
        .iter()
        .filter_map(|hit| {
            if x_layer.contains(&hit.layer) {
                Some(hit.hit_id as u32)
            } else {
                None
            }
        })
        .collect::<HashSet<u32>>();
    let other_ids = b
        .hits()
        .iter()
        .filter_map(|hit| {
            if x_layer.contains(&hit.layer) {
                Some(hit.hit_id as u32)
            } else {
                None
            }
        })
        .collect::<HashSet<u32>>();
    own_ids.intersection(&other_ids).cloned().into_iter().collect()
}
