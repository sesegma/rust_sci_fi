use crate::data_handler::Precision;
pub const DXDY: Precision = 0.087_489;
pub const Z_REFERENCE: Precision = 8520.0;
pub const DZDY: Precision = 0.003_601;
pub const LAYERS: [Precision; 12] = [
    7826.0, 7895.0, 7966.0, 8035.0, 8508.0, 8577.0, 8648.0, 8717.0, 9193.0, 9262.0, 9333.0, 9402.0,
];
