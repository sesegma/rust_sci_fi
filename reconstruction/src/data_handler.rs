#![allow(non_snake_case)]
#![allow(dead_code)]
pub mod helper_structs;
pub mod hit_combinations;
pub use helper_structs::{PairedXHit, UHit, VHit, Xpair};
pub use hit_combinations::*;

use crate::args::Parameters;
use crate::competition::*;
use crate::constants::*;
use colored::*;
use rayon::prelude::*;
use std::collections::HashMap;
use std::error::Error;
use std::fmt;
use std::fs::File;
use std::hash::{Hash, Hasher};
use std::io::BufReader;

pub type Precision = f32;
pub type KeyType = i16;

/// A struct that holds data from a single event.
#[derive(Serialize, Deserialize, Debug)]
pub struct EventData {
    pub odin: (u32, u32),
    pub MC: MC,
    pub FT: FThits,
    #[serde(skip)]
    pub params: Parameters,
    #[serde(skip)]
    pub hit_zone: u8,
}

/// Holds reconstructed FT hits
#[derive(Debug, Deserialize, Serialize)]
pub struct FThits {
    pub hits: Vec<FTHit>,
}

/// Represents a single reconstructed hit, main algorithm
/// works with these to search for matches and calculate extra
/// values.
#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct FTHit {
    pub layer: u8,
    pub hit_zone: u8,
    pub hit_id: u32,
    pub x0: Precision,
    pub z0: Precision,
    pub w: Precision,
    pub dxdy: Precision,
    pub dzdy: Precision,
    pub yMin: Precision,
    pub yMax: Precision,
    pub mcp_key: KeyType,
}

impl PartialEq for FTHit {
    fn eq(&self, other: &Self) -> bool {
        self.hit_id == other.hit_id // && self.hit_zone == other.hit_zone
    }
}
impl Eq for FTHit {}

impl FTHit {
    fn quadrant(&self) -> Quadrant {
        match (self.x0 >= 0.0, self.hit_zone == 1) {
            (true, true) => First,
            (true, false) => Fourth,
            (false, true) => Second,
            (false, false) => Third,
        }
    }
}

impl Hash for FTHit {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hit_id.hash(state);
    }
}
/// Represents four quadrants of the cartesian coordinate system
#[derive(Debug, PartialEq)]
enum Quadrant {
    First,
    Second,
    Third,
    Fourth,
}

use Quadrant::*;

/// Simulated detection of a hit
#[derive(Debug, Deserialize, Serialize)]
pub struct McFTHit {
    pub mcp_key: KeyType,
    pub x: Precision,
    pub y: Precision,
    pub z: Precision,
    #[serde(skip)]
    pub layer: u8,
}

impl McFTHit {
    pub fn add_layer(&mut self) {
        if (self.z - LAYERS[0]).abs() < 10.0 {
            self.layer = 0;
        } else if (self.z - LAYERS[1]).abs() < 10.0 {
            self.layer = 1;
        } else if (self.z - LAYERS[2]).abs() < 10.0 {
            self.layer = 2;
        } else if (self.z - LAYERS[3]).abs() < 10.0 {
            self.layer = 3;
        } else if (self.z - LAYERS[4]).abs() < 10.0 {
            self.layer = 4;
        } else if (self.z - LAYERS[5]).abs() < 10.0 {
            self.layer = 5;
        } else if (self.z - LAYERS[6]).abs() < 10.0 {
            self.layer = 6;
        } else if (self.z - LAYERS[7]).abs() < 10.0 {
            self.layer = 7;
        } else if (self.z - LAYERS[8]).abs() < 10.0 {
            self.layer = 8;
        } else if (self.z - LAYERS[9]).abs() < 10.0 {
            self.layer = 9;
        } else if (self.z - LAYERS[10]).abs() < 10.0 {
            self.layer = 10;
        } else if (self.z - LAYERS[11]).abs() < 10.0 {
            self.layer = 11;
        }
    }
}

/// A Monte Carlo simulated particle
#[derive(Debug, Deserialize, Serialize)]
pub struct MCParticle {
    pub key: KeyType,
    // #[serde(skip_deserializing)]
    pub from_s: bool,
    pub from_c: bool,
    pub from_b: bool,
    pub particleID: i32,
    pub px: Precision,
    pub py: Precision,
    pub pz: Precision,
    pub x: Precision,
    pub y: Precision,
    pub z: Precision,
    // #[serde(skip_deserializing)]
    pub flag_rec: bool,
}

/// A container of MC simulated and reconstructed particles
#[derive(Debug, Deserialize, Serialize)]
pub struct MC {
    pub particles: Vec<MCParticle>,
    pub FT_hits: Vec<McFTHit>,
}

#[derive(Debug, Clone)]
pub struct EventRunError {
    pub message: String,
}

impl fmt::Display for EventRunError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.message.bold().red())
    }
}

impl Error for EventRunError {
    fn description(&self) -> &str {
        &self.message
    }
}

impl EventData {
    /// Read event's data from a .json file specified by `filepath` and
    /// parameters from optional .yml file
    pub fn from_file<P: AsRef<std::path::Path>>(
        filepath: P,
        hit_zone: u8,
        params: &Parameters,
    ) -> Result<Self, std::io::Error> {
        let file = File::open(filepath)?;
        let reader = BufReader::new(file);
        let mut data: EventData = serde_json::from_reader(reader)?;
        data.hit_zone = hit_zone;
        // println!("Hit zone: {}", data.hit_zone);
        data.params = params.clone();
        data.MC.FT_hits.iter_mut().for_each(|hit| hit.add_layer());
        Ok(data)
    }

    fn hits_in_halflayer(&self, layer: u8, hit_zone: u8) -> impl Iterator<Item = &FTHit> {
        self.FT.hits.iter().filter(move |hit| hit.layer == layer && hit.hit_zone == hit_zone)
    }

    fn interscept_cut_off(&self, station: u8) -> Precision {
        match station {
            0 => self.params.intercept_limit.0,
            1 => self.params.intercept_limit.1,
            _ => self.params.intercept_limit.2,
        }
    }

    fn layer_y_window(&self, layer: u8) -> Precision {
        match layer {
            1 => self.params.y_limit_uv.0,
            2 => self.params.y_limit_uv.1,
            5 => self.params.y_limit_uv.2,
            6 => self.params.y_limit_uv.3,
            9 => self.params.y_limit_uv.4,
            10 => self.params.y_limit_uv.5,
            _ => unreachable!(),
        }
    }

    pub fn remove_found_hits(&mut self, found_tracks: &[&u32]) {
        for id in found_tracks.iter() {
            let hit_ind = self.FT.hits.iter().position(|hit| hit.hit_id == **id);
            if let Some(index) = hit_ind {
                self.FT.hits.remove(index);
            }
        }
    }

    // pub fn find_x_pairs(
    //     &self,
    //     station: u8,
    //     // layer_downsream: u8,
    //     // layer_upstream: u8,
    // ) -> Result<Vec<Xpair>, EventRunError> {
    //     let layer_upstream = station * 2;
    //     let layer_downsream = station * 2 + 3;
    //     let upstream_hits: Vec<_> = self.hits_in_halflayer(layer_upstream, self.hit_zone).collect();
    //     // let station = layer_upstream / 4;
    //     let mut pairs = Vec::new();
    //     for hit in self.hits_in_halflayer(layer_downsream, self.hit_zone) {
    //         let pair = Xpair::new(hit, Vec::new());
    //         pairs.push(pair);
    //     }
    //     let windows = match station {
    //         2 => self.params.windows_3,
    //         1 => self.params.windows_2,
    //         0 => self.params.windows_1,
    //         _ => unreachable!(),
    //     };

    //     pairs.par_iter_mut().for_each(move |xpair| {
    //         let (
    //             min_corridor_central,
    //             max_corridor_central,
    //             min_corridor_peripheral,
    //             max_corridor_peripheral,
    //         ) = match xpair.orig_hit.quadrant() {
    //             First | Fourth => (windows.0, windows.1, windows.2, windows.3),
    //             Second | Third => (windows.1, windows.0, windows.3, windows.2),
    //         };
    //         let (loc_min, loc_max) = if xpair.orig_hit.x0.abs() <= self.params.segment_cut {
    //             (xpair.orig_hit.x0 - max_corridor_central, xpair.orig_hit.x0 + min_corridor_central)
    //         } else {
    //             (
    //                 xpair.orig_hit.x0 - max_corridor_peripheral,
    //                 xpair.orig_hit.x0 + min_corridor_peripheral,
    //             )
    //         };
    //         let mut paired_hits: Vec<_> = upstream_hits
    //             .iter()
    //             .filter_map(|h| {
    //                 if h.x0 >= loc_min && h.x0 <= loc_max {
    //                     let tx = (xpair.orig_hit.x0 - h.x0) / (xpair.orig_hit.z0 - h.z0);
    //                     Some(PairedXHit {
    //                         tx,
    //                         hit: *h,
    //                         hits_v: vec![],
    //                     })
    //                 } else {
    //                     None
    //                 }
    //             })
    //             .collect();

    //         if !paired_hits.is_empty() {
    //             xpair.pairs.append(&mut paired_hits);
    //         }
    //     });
    //     if pairs.is_empty() {
    //         return Err(EventRunError {
    //             message: "Couldn't make Xpairs".to_string(),
    //         });
    //     }
    //     Ok(pairs)
    // }

    pub fn find_pairs(
        &self,
        station: u8,
        // layer_upstream: u8,
    ) -> Result<Vec<Pair>, EventRunError> {
        let layer_upstream = station * 2;
        let layer_downsream = station * 2 + 3;
        let upstream_hits: Vec<_> = self.hits_in_halflayer(layer_upstream, self.hit_zone).collect();
        let downstream_hits: Vec<_> =
            self.hits_in_halflayer(layer_downsream, self.hit_zone).collect();
        let station = layer_upstream / 4;
        let mut pairs = vec![vec![]; downstream_hits.len()];
        let windows = match station {
            2 => self.params.windows_3,
            1 => self.params.windows_2,
            0 => self.params.windows_1,
            _ => unreachable!(),
        };

        pairs.par_iter_mut().enumerate().for_each(|(i, res)| {
            let (
                min_corridor_central,
                max_corridor_central,
                min_corridor_peripheral,
                max_corridor_peripheral,
            ) = match downstream_hits[i].quadrant() {
                First | Fourth => (windows.0, windows.1, windows.2, windows.3),
                Second | Third => (windows.1, windows.0, windows.3, windows.2),
            };
            let (loc_min, loc_max) = if downstream_hits[i].x0.abs() <= self.params.segment_cut {
                (
                    downstream_hits[i].x0 - max_corridor_central,
                    downstream_hits[i].x0 + min_corridor_central,
                )
            } else {
                (
                    downstream_hits[i].x0 - max_corridor_peripheral,
                    downstream_hits[i].x0 + min_corridor_peripheral,
                )
            };
            let ind_min =
                match upstream_hits.binary_search_by(|hit| hit.x0.partial_cmp(&loc_min).unwrap()) {
                    Ok(ind) => ind,
                    Err(ind) => ind,
                };
            let ind_max =
                match upstream_hits.binary_search_by(|hit| hit.x0.partial_cmp(&loc_max).unwrap()) {
                    Ok(ind) => ind,
                    Err(ind) => ind,
                };

            let mut paired_hits: Vec<_> = if ind_max != ind_min {
                upstream_hits[ind_min..ind_max]
                    .iter()
                    .map(|u_hit| {
                        let tx =
                            (downstream_hits[i].x0 - u_hit.x0) / (downstream_hits[i].z0 - u_hit.z0);
                        Pair {
                            hit1: u_hit,
                            hit4: downstream_hits[i],
                            tx,
                            chi2_x: 10_000.0,
                        }
                    })
                    .collect()
            } else {
                vec![]
            };
            if !paired_hits.is_empty() {
                res.append(&mut paired_hits);
            }
        });
        if pairs.is_empty() {
            return Err(EventRunError {
                message: "Couldn't find pairs".to_string(),
            });
        }
        Ok(pairs.into_iter().flatten().collect::<Vec<Pair>>())
    }

    /// Finds quadruplets in a station.
    pub fn par_find_quadruplets(&self, station: u8) -> Result<Vec<Quadruplet>, EventRunError> {
        let (layer_1, layer_2, layer_3, layer_4) =
            (station * 4, station * 4 + 1, station * 4 + 2, station * 4 + 3);
        let hits_1: Vec<_> = self.hits_in_halflayer(layer_1, self.hit_zone).collect();
        let hits_2: Vec<_> = self.hits_in_halflayer(layer_2, self.hit_zone).collect();
        let hits_3: Vec<_> = self.hits_in_halflayer(layer_3, self.hit_zone).collect();
        let hits_4: Vec<_> = self.hits_in_halflayer(layer_4, self.hit_zone).collect();

        let delta_z_1_u = hits_2[0].z0 - hits_1[0].z0;
        let delta_z_v_2 = hits_4[0].z0 - hits_3[0].z0;
        let windows = match station {
            2 => self.params.windows_3,
            1 => self.params.windows_2,
            0 => self.params.windows_1,
            _ => unreachable!(),
        };

        let v: Vec<Vec<Quadruplet>> = hits_4
            .par_iter()
            .filter_map(|hit4| {
                let hit_quadrant = hit4.quadrant();
                let (
                    min_corridor_central,
                    max_corridor_central,
                    min_corridor_peripheral,
                    max_corridor_peripheral,
                ) = match hit_quadrant {
                    First | Fourth => (windows.0, windows.1, windows.2, windows.3),
                    Second | Third => (windows.1, windows.0, windows.3, windows.2),
                };
                let (loc_min, loc_max) = if hit4.x0.abs() <= self.params.segment_cut {
                    (hit4.x0 - max_corridor_central, hit4.x0 + min_corridor_central)
                } else {
                    (hit4.x0 - max_corridor_peripheral, hit4.x0 + min_corridor_peripheral)
                };
                let ind_min =
                    match hits_1.binary_search_by(|hit| hit.x0.partial_cmp(&loc_min).unwrap()) {
                        Ok(ind) => ind,
                        Err(ind) => ind,
                    };
                let ind_max =
                    match hits_1.binary_search_by(|hit| hit.x0.partial_cmp(&loc_max).unwrap()) {
                        Ok(ind) => ind,
                        Err(ind) => ind,
                    };
                let mut matches_1 = if ind_max != ind_min {
                    hits_1[ind_min..ind_max]
                        .iter()
                        .map(|hit1| {
                            let tx = (hit4.x0 - hit1.x0) / (hit4.z0 - hit1.z0);
                            PairedXHit::new(hit1, tx, vec![])
                        })
                        .collect()
                } else {
                    vec![]
                };
                if !matches_1.is_empty() {
                    // iterating through vector of pairs
                    matches_1.par_iter_mut().for_each(|hit1| {
                        let x_u = hit1.tx * delta_z_1_u + hit1.hit.x0;
                        let x_v = hit4.x0 - hit1.tx * delta_z_v_2;
                        let (max_wind_c, min_wind_c, max_wind_p, min_wind_p) = match hit_quadrant {
                            First | Second => (
                                x_v + self.params.stereo_windows.1,
                                x_v - self.params.stereo_windows.0,
                                x_v + self.params.stereo_windows.3,
                                x_v - self.params.stereo_windows.2,
                            ),
                            Third | Fourth => (
                                x_v + self.params.stereo_windows.0,
                                x_v - self.params.stereo_windows.1,
                                x_v + self.params.stereo_windows.2,
                                x_v - self.params.stereo_windows.3,
                            ),
                        };
                        let (min_w, max_w) = if hit4.x0.abs() <= self.params.segment_cut {
                            (min_wind_c, max_wind_c)
                        } else {
                            (min_wind_p, max_wind_p)
                        };
                        let ind_min_v = match hits_3
                            .binary_search_by(|hit| hit.x0.partial_cmp(&min_w).unwrap())
                        {
                            Ok(ind) => ind,
                            Err(ind) => ind,
                        };
                        let ind_max_v = match hits_3
                            .binary_search_by(|hit| hit.x0.partial_cmp(&max_w).unwrap())
                        {
                            Ok(ind) => ind,
                            Err(ind) => ind,
                        };
                        let mut v_matches = if ind_max_v != ind_min_v {
                            hits_3[ind_min_v..ind_max_v]
                                .iter()
                                .map(|hit3| VHit::new(hit3, x_v, vec![]))
                                .collect()
                        } else {
                            vec![]
                        };
                        if !v_matches.is_empty() {
                            hit1.hits_v.append(&mut v_matches);
                            hit1.hits_v.par_iter_mut().for_each(|hit_v| {
                                let y_v = (x_v - hit_v.hit.x0) / -DXDY;
                                let (x_u_max, x_u_min) = match hit_quadrant {
                                    First | Second => (
                                        x_u - (y_v - self.params.y_u_windows.1) * DXDY,
                                        x_u - (y_v + self.params.y_u_windows.0) * DXDY,
                                    ),
                                    Third | Fourth => (
                                        x_u + (y_v - self.params.y_u_windows.0) * (-DXDY),
                                        x_u + (y_v + self.params.y_u_windows.1) * (-DXDY),
                                    ),
                                };
                                let ind_min_u = match hits_2
                                    .binary_search_by(|hit| hit.x0.partial_cmp(&x_u_min).unwrap())
                                {
                                    Ok(ind) => ind,
                                    Err(ind) => ind,
                                };
                                let ind_max_u = match hits_2
                                    .binary_search_by(|hit| hit.x0.partial_cmp(&x_u_max).unwrap())
                                {
                                    Ok(ind) => ind,
                                    Err(ind) => ind,
                                };
                                let mut u_matches = if ind_max_u != ind_min_u {
                                    hits_2[ind_min_u..ind_max_u]
                                        .par_iter()
                                        .filter_map(|hit2| {
                                            let y_u = (x_u - hit2.x0) / DXDY;
                                            let ty = (y_v - y_u) / (hit_v.hit.z0 - hit2.z0);
                                            // let z_u = hit2.z0 + y_u * DZDY;
                                            // let z_v = hit_v.hit.z0 + y_v * DZDY;
                                            if y_v.abs() < self.layer_y_window(layer_3)
                                                && y_u.abs() < self.layer_y_window(layer_2)
                                                && (y_v - ty * hit2.z0).abs()
                                                    < self.interscept_cut_off(station)
                                            {
                                                Some(UHit::new(hit2, x_u, y_u, y_v, ty))
                                            } else {
                                                None
                                            }
                                        })
                                        .collect()
                                } else {
                                    vec![]
                                };
                                if !u_matches.is_empty() {
                                    hit_v.hits_u.append(&mut u_matches);
                                }
                            })
                        }
                    });
                    Some(
                        Xpair::new(hit4, matches_1)
                            .pairs
                            .par_iter()
                            .flat_map(move |hit1| {
                                hit1.hits_v.par_iter().flat_map(move |hit3| {
                                    hit3.hits_u.par_iter().filter_map(move |hit2| {
                                        Some(Quadruplet {
                                            hit4,
                                            hit3: hit3.hit,
                                            hit2: hit2.hit,
                                            hit1: hit1.hit,
                                            x_u: hit2.x_u,
                                            x_v: hit3.x_v,
                                            tx: hit1.tx,
                                            ty: hit2.ty,
                                            y_u: hit2.y_u,
                                            y_v: hit2.y_v,
                                            // z_u: hit2.z_u,
                                            // z_v: hit2.z_v,
                                        })
                                    })
                                })
                            })
                            .collect(),
                    )
                } else {
                    None
                }
            })
            .collect();
        if v.is_empty() {
            return Err(EventRunError {
                message: "Couldn't build quadruplets".to_string(),
            });
        }
        Ok(v.into_iter().flatten().collect::<Vec<Quadruplet>>())
    }

    pub fn get_octuplets<'a>(
        &self,
        quads_down: &'a [Quadruplet<'a>],
        quads_up: &'a [Quadruplet<'a>],
    ) -> Result<Vec<Octuplet<'a>>, EventRunError> {
        match (quads_down.is_empty(), quads_up.is_empty()) {
            (true, true) => {
                return Err(EventRunError {
                    message: "Can't build octuplets, both quadruplet vectors are empty."
                        .to_string(),
                });
            }
            (false, true) => {
                return Err(EventRunError {
                    message: format!("Can't build octuplets, {} is empty.", stringify!(quads_down)),
                });
            }
            (true, false) => {
                return Err(EventRunError {
                    message: format!("Can't build octuplets, {} is empty.", stringify!(quads_up)),
                });
            }
            _ => {}
        }
        let dz_stations = quads_down[0].hit1.z0 - quads_up[0].hit4.z0;
        // let dz_v_layers = quads_down[0].hit3.z0 - quads_up[0].hit3.z0;
        let mut result = vec![vec![]; quads_down.len()];
        let chi2_x_cut = match quads_up[0].hit1.layer / 4 {
            1 | 2 => self.params.chi2_x_cut_23,
            0 => self.params.chi2_x_cut_1,
            _ => unreachable!(),
        };

        result.par_iter_mut().enumerate().for_each(|(i, res)| {
            // result.iter_mut().enumerate().for_each(|(i, res)| {
            let quad_d: &Quadruplet = &quads_down[i];
            let x_proj = quad_d.hit1.x0 - quad_d.tx * dz_stations;
            let loc_min = x_proj - self.params.x_interval_2;
            let loc_max = x_proj + self.params.x_interval_2;
            let ind_min = match quads_up
                .binary_search_by(|quad| quad.hit4.x0.partial_cmp(&loc_min).unwrap())
            {
                Ok(ind) => ind,
                Err(ind) => ind,
            };
            let ind_max = match quads_up
                .binary_search_by(|quad| quad.hit4.x0.partial_cmp(&loc_max).unwrap())
            {
                Ok(ind) => ind,
                Err(ind) => ind,
            };
            let mut vec_quads_up = if ind_max != ind_min {
                quads_up[ind_min..ind_max]
                    .par_iter()
                    .filter_map(|q_up| {
                        let (chi2_x_parabolic, chi2_y_parabolic, track_params) =
                            Octuplet::chi2_parabolic(&q_up, &quad_d, 5, 2);
                        // let (_chi2_x_linear, _chi2_y_linear, track_params_linear) =
                        //     Octuplet::chi2_linear(&q_up, &quad_d, 5, 2);
                        if chi2_x_parabolic < chi2_x_cut
                            && chi2_y_parabolic < self.params.chi2_y_cut
                            && (q_up.tx - quad_d.tx).abs() < self.params.tx_cut
                            && (q_up.ty - quad_d.ty).abs() < self.params.ty_cut
                        {
                            let (true_track, majority_key) =
                                Self::true_oct_track(&q_up, &quads_down[i]);
                            // dbg!((chi2_x, chi2_y, true_track));
                            Some(Octuplet {
                                quad_down: &quads_down[i],
                                quad_up: &q_up,
                                chi2_x: chi2_x_parabolic,
                                chi2_y: chi2_y_parabolic,
                                track_params,
                                track_params_linear: TrackParameters::default(),
                                true_track,
                                majority_key,
                            })
                        } else {
                            None
                        }
                    })
                    .collect()
            } else {
                vec![]
            };

            if !vec_quads_up.is_empty() {
                vec_quads_up = if self.params.oct_competition.0 {
                    track_competition(
                        &vec_quads_up,
                        self.params.oct_competition.1,
                        Box::new(hits_in_common),
                        Box::new(find_winners),
                        // Box::new(find_single_winner),
                    )
                } else {
                    vec_quads_up
                };
                res.append(&mut vec_quads_up);
            }
        });
        let result = result.into_iter().flatten().collect::<Vec<Octuplet>>();
        if result.is_empty() {
            return Err(EventRunError {
                message: "Couldn't build octuplets".to_string(),
            });
        }
        if result.len() > self.params.max_len_oct_tracks {
            return Err(EventRunError {
                message: format!(
                    "Number of oct tracks exceeds set limit of {}",
                    self.params.max_len_oct_tracks
                ),
            });
        }
        Ok(result)
    }

    pub fn get_full_tracks<'a>(
        &self,
        octs: &'a [Octuplet<'a>],
        quads: &'a [Quadruplet<'a>],
    ) -> Result<Vec<FullTrack<'a>>, EventRunError> {
        match (quads.is_empty(), octs.is_empty()) {
            (true, true) => {
                return Err(EventRunError {
                    message:
                        "Can't build full tracks, both quadruplet and octuplet vectors are empty."
                            .to_string(),
                });
            }
            (false, true) => {
                return Err(EventRunError {
                    message: "Can't build full tracks, {} is empty.".to_string(),
                });
            }
            (true, false) => {
                return Err(EventRunError {
                    message: "Can't build full tracks, {} is empty.".to_string(),
                });
            }
            _ => {}
        }
        // let dz_stations = octs[0].quad_up.hit1.z0 - quads[0].hit4.z0;
        let z0 = quads[0].hit4.z0 - Z_REFERENCE;
        // let mut result = vec![Vec::with_capacity(50); octs.len()];
        let mut result = vec![vec![]; octs.len()];
        result.par_iter_mut().enumerate().for_each(|(i, res)| {
            // let quad_2: &Quadruplet = &octs[i].quad_up;
            // let x_proj = quad_2.hit1.x0 - octs[i].track_params.x_slope * dz_stations;
            let x_proj_parabolic = octs[i].track_params.x_intercept
                // + (octs[i].track_params.x_slope * 0.993 + (octs[i].track_params.x_quadratic * z0))
                + (octs[i].track_params.x_slope + (octs[i].track_params.x_quadratic * 1.96 * z0))
                    * z0;
            // + 0.5 * (octs[i].track_params.x_quadratic.signum())
            // + 0.1 * (octs[i].track_params.x_slope.signum());
            let x_proj_linear =
                octs[i].track_params_linear.x_intercept + octs[i].track_params_linear.x_slope * z0;
            let loc_min = x_proj_parabolic - self.params.x_interval_1;
            let loc_max = x_proj_parabolic + self.params.x_interval_1;
            // let y_proj = quad_d.y_v - quad_d.beta * dz_v_layers;
            let ind_min = match quads
                .binary_search_by(|quad_1| quad_1.hit4.x0.partial_cmp(&loc_min).unwrap())
            {
                Ok(ind) => ind,
                Err(ind) => ind,
            };
            let ind_max = match quads
                .binary_search_by(|quad_1| quad_1.hit4.x0.partial_cmp(&loc_max).unwrap())
            {
                Ok(ind) => ind,
                Err(ind) => ind,
            };
            let mut vec_quads = if ind_max != ind_min {
                quads[ind_min..ind_max]
                    .par_iter()
                    .filter_map(|quad_1| {
                        let track_params = FullTrack::fit_parabola(&quad_1, &octs[i]);
                        // number of degrees of freedom is (number of coordinates to fit - number
                        // of vfree parameters)
                        let (chi2_x, chi2_y) =
                            FullTrack::chi2_parabolic(&track_params, &quad_1, &octs[i], 9, 4);
                        if (quad_1.tx - track_params.x_slope).abs() < self.params.tx_cut
                            && (quad_1.ty - track_params.y_slope).abs() < self.params.ty_cut
                            && chi2_x < self.params.chi2_x_cut_1
                            && chi2_y < self.params.chi2_y_cut_1
                        //self.params.chi2_y_cut
                        {
                            let (true_track, majority_key) = Self::true_full_track(
                                &quad_1,
                                &octs[i].quad_up,
                                &octs[i].quad_down,
                            );
                            // dbg!(x_proj_parabolic, x_proj_linear);
                            Some(FullTrack {
                                quad_1: &quad_1,
                                quad_2: &octs[i].quad_up,
                                quad_3: &octs[i].quad_down,
                                track_params,
                                chi2_x,
                                chi2_y,
                                true_track,
                                majority_key,
                                x_proj_parabolic,
                                x_proj_linear,
                                tx_parabolic: octs[i].track_params.x_slope,
                                tx_linear: octs[i].track_params_linear.x_slope,
                            })
                        } else {
                            None
                        }
                    })
                    .collect()
            } else {
                vec![]
            };

            if !vec_quads.is_empty() {
                vec_quads = if self.params.full_competition.0 {
                    track_competition(
                        &vec_quads,
                        self.params.full_competition.1,
                        Box::new(hits_in_common),
                        Box::new(find_winners_by_chi2_x),
                    )
                } else {
                    vec_quads
                };
                res.append(&mut vec_quads);
            }
        });
        let result: Vec<_> = result.into_iter().flatten().collect();
        if result.is_empty() {
            return Err(EventRunError {
                message: "Couldn't build full tracks".to_string(),
            });
        }
        if result.len() > self.params.max_len_full_tracks {
            return Err(EventRunError {
                message: format!(
                    "Number of full tracks exceeds set limit of {}",
                    self.params.max_len_full_tracks
                ),
            });
        }
        Ok(result)
    }

    fn true_full_track(
        quad_1: &Quadruplet,
        quad_up: &Quadruplet,
        quad_down: &Quadruplet,
    ) -> (bool, Option<KeyType>) {
        let mut key_set = HashMap::new();
        *key_set.entry(quad_1.hit1.mcp_key).or_insert(0) += 1;
        *key_set.entry(quad_1.hit2.mcp_key).or_insert(0) += 1;
        *key_set.entry(quad_1.hit3.mcp_key).or_insert(0) += 1;
        *key_set.entry(quad_1.hit4.mcp_key).or_insert(0) += 1;
        *key_set.entry(quad_up.hit1.mcp_key).or_insert(0) += 1;
        *key_set.entry(quad_up.hit2.mcp_key).or_insert(0) += 1;
        *key_set.entry(quad_up.hit3.mcp_key).or_insert(0) += 1;
        *key_set.entry(quad_up.hit4.mcp_key).or_insert(0) += 1;
        *key_set.entry(quad_down.hit1.mcp_key).or_insert(0) += 1;
        *key_set.entry(quad_down.hit2.mcp_key).or_insert(0) += 1;
        *key_set.entry(quad_down.hit3.mcp_key).or_insert(0) += 1;
        *key_set.entry(quad_down.hit4.mcp_key).or_insert(0) += 1;
        // if key_set.iter().any(|(_key, &val)| val >= 9 && val > 0){ }
        for (&key, &val) in key_set.iter() {
            if val >= 9 && key != -1 {
                return (true, Some(key));
            }
        }
        (false, None)
    }
    fn true_oct_track(quad_up: &Quadruplet, quad_down: &Quadruplet) -> (bool, Option<KeyType>) {
        let mut key_set = HashMap::new();
        *key_set.entry(quad_up.hit1.mcp_key).or_insert(0) += 1;
        *key_set.entry(quad_up.hit2.mcp_key).or_insert(0) += 1;
        *key_set.entry(quad_up.hit3.mcp_key).or_insert(0) += 1;
        *key_set.entry(quad_up.hit4.mcp_key).or_insert(0) += 1;
        *key_set.entry(quad_down.hit1.mcp_key).or_insert(0) += 1;
        *key_set.entry(quad_down.hit2.mcp_key).or_insert(0) += 1;
        *key_set.entry(quad_down.hit3.mcp_key).or_insert(0) += 1;
        *key_set.entry(quad_down.hit4.mcp_key).or_insert(0) += 1;
        // if key_set.iter().any(|(_key, &val)| val >= 9 && val > 0){ }
        for (&key, &val) in key_set.iter() {
            if val >= 6 && key != -1 {
                return (true, Some(key));
            }
        }
        (false, None)
    }
}
